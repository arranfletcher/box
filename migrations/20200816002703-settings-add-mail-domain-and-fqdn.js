'use strict';

const async = require('async');

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="admin_domain"', function (error, results) {
        if (error || results.length === 0) return callback(error);

        const adminDomain = results[0].value;

        async.series([
            db.runSql.bind(db, 'INSERT INTO settings (name, value) VALUES (?, ?)', [ 'mail_domain', adminDomain ]),
            db.runSql.bind(db, 'INSERT INTO settings (name, value) VALUES (?, ?)', [ 'mail_fqdn', `my.${adminDomain}` ])
        ], callback);
    });
};

exports.down = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'DELETE FROM settings WHERE name="mail_domain"'),
        db.runSql.bind(db, 'DELETE FROM settings WHERE name="mail_fqdn"'),
    ], callback);
};
