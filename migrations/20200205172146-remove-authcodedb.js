'use strict';

exports.up = function(db, callback) {
    db.runSql('DROP TABLE authcodes', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    var cmd = `CREATE TABLE IF NOT EXISTS authcodes(
        authCode VARCHAR(128) NOT NULL UNIQUE,
        userId VARCHAR(128) NOT NULL,
        clientId VARCHAR(128) NOT NULL,
        expiresAt BIGINT NOT NULL,
        PRIMARY KEY(authCode)) CHARACTER SET utf8 COLLATE utf8_bin`;

    db.runSql(cmd, function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
