'use strict';

exports.up = function(db, callback) {
    var cmd1 = 'CREATE TABLE volumes(' +
                'id VARCHAR(128) NOT NULL UNIQUE,' +
                'name VARCHAR(256) NOT NULL UNIQUE,' +
                'hostPath VARCHAR(1024) NOT NULL UNIQUE,' +
                'creationTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
                'PRIMARY KEY (id)) CHARACTER SET utf8 COLLATE utf8_bin';

    var cmd2 = 'CREATE TABLE appMounts(' +
                 'appId VARCHAR(128) NOT NULL,' +
                 'volumeId VARCHAR(128) NOT NULL,' +
                 'readOnly BOOLEAN DEFAULT 1,' +
                 'UNIQUE KEY appMounts_appId_volumeId (appId, volumeId),' +
                 'FOREIGN KEY(appId) REFERENCES apps(id),' +
                 'FOREIGN KEY(volumeId) REFERENCES volumes(id)) CHARACTER SET utf8 COLLATE utf8_bin;';

    db.runSql(cmd1, function (error) {
        if (error) console.error(error);

        db.runSql(cmd2, function (error) {
            if (error) console.error(error);

            db.runSql('ALTER TABLE apps DROP COLUMN bindsJson', callback);
        });
    });
};

exports.down = function(db, callback) {
    db.runSql('DROP TABLE appMounts', function (error) {
        if (error) console.error(error);

        db.runSql('DROP TABLE volumes', function (error) {
            if (error) console.error(error);
            callback(error);
        });
    });
};

