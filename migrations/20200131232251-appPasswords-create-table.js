'use strict';

exports.up = function(db, callback) {
	var cmd = 'CREATE TABLE appPasswords(' +
                'id VARCHAR(128) NOT NULL UNIQUE,' +
				'name VARCHAR(128) NOT NULL,' +
                'userId VARCHAR(128) NOT NULL,' +
                'identifier VARCHAR(128) NOT NULL,' +
                'hashedPassword VARCHAR(1024) NOT NULL,' +
				'creationTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
                'FOREIGN KEY(userId) REFERENCES users(id),' +
                'UNIQUE (name, userId),' +
				'PRIMARY KEY (id)) CHARACTER SET utf8 COLLATE utf8_bin';

    db.runSql(cmd, function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('DROP TABLE appPasswords', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
