#!/usr/bin/env node

'use strict';

var apptask = require('./apptask.js'),
    async = require('async'),
    backups = require('./backups.js'),
    cloudron = require('./cloudron.js'),
    database = require('./database.js'),
    externalLdap = require('./externalldap.js'),
    fs = require('fs'),
    mail = require('./mail.js'),
    reverseProxy = require('./reverseproxy.js'),
    settings = require('./settings.js'),
    tasks = require('./tasks.js'),
    updater = require('./updater.js'),
    util = require('util');

const TASKS = { // indexed by task type
    app: apptask.run,
    backup: backups.backupBoxAndApps,
    update: updater.update,
    renewcerts: reverseProxy.renewCerts,
    setupDnsAndCert: cloudron.setupDnsAndCert,
    cleanBackups: backups.cleanup,
    syncExternalLdap: externalLdap.sync,
    changeMailLocation: mail.changeLocation,

    _identity: (arg, progressCallback, callback) => callback(null, arg),
    _error: (arg, progressCallback, callback) => callback(new Error(`Failed for arg: ${arg}`)),
    _crash: (arg) => { throw new Error(`Crashing for arg: ${arg}`); }, // the test looks for this debug string in the log file
    _sleep: (arg) => setTimeout(process.exit, arg)
};

if (process.argv.length !== 4) {
    console.error('Pass the taskid and logfile as argument');
    process.exit(1);
}

const taskId = process.argv[2];
const logFile = process.argv[3];

function setupLogging(callback) {
    fs.open(logFile, 'a', function (error, fd) {
        if (error) return callback(error);

        require('debug').log = function (...args) {
            fs.appendFileSync(fd, util.format(...args) + '\n');
        };

        callback();
    });
}

// Main process starts here
const startTime = new Date();

async.series([
    setupLogging,
    database.initialize,
    settings.initCache
], function (error) {
    if (error) return process.exit(50);

    const debug = require('debug')('box:taskworker'); // require this here so that logging handler is already setup
    const NOOP_CALLBACK = function (error) { if (error) debug(error); };

    process.on('SIGTERM', () => process.exit(0)); // sent as timeout notification

    // ensure we log task crashes with the task logs
    process.on('uncaughtException', function (e) { debug(e); process.exit(1); });

    debug(`Starting task ${taskId}. Logs are at ${logFile}`);

    tasks.get(taskId, function (error, task) {
        if (error) return process.exit(50);

        tasks.update(taskId, { percent: 2, error: null }, function (error) {
            if (error) {
                console.error(error);
                return process.exit(50);
            }

            const progressCallback = (progress, cb) => tasks.update(taskId, progress, cb || NOOP_CALLBACK);
            const resultCallback = (error, result) => {
                // Error object has properties with enumerable: false (https://mattcbaker.com/posts/stringify-javascript-error/)
                const progress = {
                    result: result || null,
                    error: error ? JSON.parse(JSON.stringify(error, Object.getOwnPropertyNames(error))) : null
                };

                debug(`Task took ${(new Date() - startTime)/1000} seconds`);

                tasks.setCompleted(taskId, progress, () => process.exit(error ? 50 : 0));
            };

            try {
                TASKS[task.type].apply(null, task.args.concat(progressCallback).concat(resultCallback));
            } catch (error) {
                debug('Uncaught exception in task', error);
                process.exit(1); // do not call setCompleted() intentionally. the task code must be resilient enough to handle it
            }
        });

    });
});
