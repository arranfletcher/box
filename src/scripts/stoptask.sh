#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

task_id="$1"

if [[ "${task_id}" == "all" ]]; then
    systemctl list-units --full --no-legend box-task-* # just to show who was running
    systemctl kill --signal=SIGTERM box-task-* || true
    systemctl reset-failed box-task-* 2>/dev/null || true
    systemctl stop box-task-* || true # because of remain-after-exit in Ubuntu 16 we have to deactivate the service
else
    readonly service_name="box-task-${task_id}"
    systemctl kill --signal=SIGTERM "${service_name}" || true
    systemctl stop "${service_name}" || true # because of remain-after-exit in Ubuntu 16 we have to deactivate the service
fi
