'use strict';

exports = module.exports = {
    add: add,
    get: get,
    getAllPaged: getAllPaged,
    getByCreationTime: getByCreationTime,
    cleanup: cleanup,

    // keep in sync with webadmin index.js filter
    ACTION_ACTIVATE: 'cloudron.activate',
    ACTION_APP_CLONE: 'app.clone',
    ACTION_APP_CONFIGURE: 'app.configure',
    ACTION_APP_REPAIR: 'app.repair',
    ACTION_APP_INSTALL: 'app.install',
    ACTION_APP_RESTORE: 'app.restore',
    ACTION_APP_UNINSTALL: 'app.uninstall',
    ACTION_APP_UPDATE: 'app.update',
    ACTION_APP_UPDATE_FINISH: 'app.update.finish',
    ACTION_APP_LOGIN: 'app.login',
    ACTION_APP_OOM: 'app.oom',
    ACTION_APP_UP: 'app.up',
    ACTION_APP_DOWN: 'app.down',
    ACTION_APP_START: 'app.start',
    ACTION_APP_STOP: 'app.stop',
    ACTION_APP_RESTART: 'app.restart',

    ACTION_BACKUP_FINISH: 'backup.finish',
    ACTION_BACKUP_START: 'backup.start',
    ACTION_BACKUP_CLEANUP_START: 'backup.cleanup.start', // obsolete
    ACTION_BACKUP_CLEANUP_FINISH: 'backup.cleanup.finish',

    ACTION_CERTIFICATE_NEW: 'certificate.new',
    ACTION_CERTIFICATE_RENEWAL: 'certificate.renew',

    ACTION_DASHBOARD_DOMAIN_UPDATE: 'dashboard.domain.update',

    ACTION_DOMAIN_ADD: 'domain.add',
    ACTION_DOMAIN_UPDATE: 'domain.update',
    ACTION_DOMAIN_REMOVE: 'domain.remove',

    ACTION_MAIL_LOCATION: 'mail.location',
    ACTION_MAIL_ENABLED: 'mail.enabled',
    ACTION_MAIL_DISABLED: 'mail.disabled',
    ACTION_MAIL_MAILBOX_ADD: 'mail.box.add',
    ACTION_MAIL_MAILBOX_REMOVE: 'mail.box.remove',
    ACTION_MAIL_MAILBOX_UPDATE: 'mail.box.update',
    ACTION_MAIL_LIST_ADD: 'mail.list.add',
    ACTION_MAIL_LIST_REMOVE: 'mail.list.remove',
    ACTION_MAIL_LIST_UPDATE: 'mail.list.update',

    ACTION_PROVISION: 'cloudron.provision',
    ACTION_RESTORE: 'cloudron.restore', // unused
    ACTION_START: 'cloudron.start',
    ACTION_UPDATE: 'cloudron.update',
    ACTION_UPDATE_FINISH: 'cloudron.update.finish',

    ACTION_USER_ADD: 'user.add',
    ACTION_USER_LOGIN: 'user.login',
    ACTION_USER_REMOVE: 'user.remove',
    ACTION_USER_UPDATE: 'user.update',
    ACTION_USER_TRANSFER: 'user.transfer',

    ACTION_VOLUME_ADD: 'volume.add',
    ACTION_VOLUME_UPDATE: 'volume.update',
    ACTION_VOLUME_REMOVE: 'volume.remove',

    ACTION_DYNDNS_UPDATE: 'dyndns.update',

    ACTION_SUPPORT_TICKET: 'support.ticket',
    ACTION_SUPPORT_SSH: 'support.ssh',

    ACTION_PROCESS_CRASH: 'system.crash'
};

var assert = require('assert'),
    debug = require('debug')('box:eventlog'),
    eventlogdb = require('./eventlogdb.js'),
    notifications = require('./notifications.js'),
    util = require('util'),
    uuid = require('uuid');

var NOOP_CALLBACK = function (error) { if (error) debug(error); };

function add(action, source, data, callback) {
    assert.strictEqual(typeof action, 'string');
    assert.strictEqual(typeof source, 'object');
    assert.strictEqual(typeof data, 'object');
    assert(!callback || typeof callback === 'function');

    callback = callback || NOOP_CALLBACK;

    // we do only daily upserts for login actions, so they don't spam the db
    var api = action === exports.ACTION_USER_LOGIN ? eventlogdb.upsert : eventlogdb.add;
    api(uuid.v4(), action, source, data, function (error, id) {
        if (error) return callback(error);

        callback(null, { id: id });

        notifications.onEvent(id, action, source, data, NOOP_CALLBACK);
    });
}

function get(id, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof callback, 'function');

    eventlogdb.get(id, function (error, result) {
        if (error) return callback(error);

        callback(null, result);
    });
}

function getAllPaged(actions, search, page, perPage, callback) {
    assert(Array.isArray(actions));
    assert(typeof search === 'string' || search === null);
    assert.strictEqual(typeof page, 'number');
    assert.strictEqual(typeof perPage, 'number');
    assert.strictEqual(typeof callback, 'function');

    eventlogdb.getAllPaged(actions, search, page, perPage, function (error, events) {
        if (error) return callback(error);

        callback(null, events);
    });
}

function getByCreationTime(creationTime, callback) {
    assert(util.isDate(creationTime));
    assert.strictEqual(typeof callback, 'function');

    eventlogdb.getByCreationTime(creationTime, function (error, events) {
        if (error) return callback(error);

        callback(null, events);
    });
}

function cleanup(callback) {
    callback = callback || NOOP_CALLBACK;

    var d = new Date();
    d.setDate(d.getDate() - 10); // 10 days ago

    eventlogdb.delByCreationTime(d, function (error) {
        if (error) return callback(error);

        callback(null);
    });
}
