'use strict';

exports = module.exports = {
    addMailbox,
    addList,

    updateMailboxOwner,
    updateList,
    del,

    getMailboxCount,
    listMailboxes,
    getLists,

    listAllMailboxes,

    get,
    getMailbox,
    getList,
    getAlias,

    getAliasesForName,
    setAliasesForName,

    getByOwnerId,
    delByOwnerId,
    delByDomain,

    updateName,

    _clear: clear,

    TYPE_MAILBOX: 'mailbox',
    TYPE_LIST: 'list',
    TYPE_ALIAS: 'alias'
};

var assert = require('assert'),
    BoxError = require('./boxerror.js'),
    database = require('./database.js'),
    mysql = require('mysql'),
    safe = require('safetydance'),
    util = require('util');

var MAILBOX_FIELDS = [ 'name', 'type', 'ownerId', 'aliasName', 'aliasDomain', 'creationTime', 'membersJson', 'membersOnly', 'domain' ].join(',');

function postProcess(data) {
    data.members = safe.JSON.parse(data.membersJson) || [ ];
    delete data.membersJson;

    data.membersOnly = !!data.membersOnly;

    return data;
}

function addMailbox(name, domain, ownerId, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof ownerId, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('INSERT INTO mailboxes (name, type, domain, ownerId) VALUES (?, ?, ?, ?)', [ name, exports.TYPE_MAILBOX, domain, ownerId ], function (error) {
        if (error && error.code === 'ER_DUP_ENTRY') return callback(new BoxError(BoxError.ALREADY_EXISTS, 'mailbox already exists'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function updateMailboxOwner(name, domain, ownerId, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof ownerId, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('UPDATE mailboxes SET ownerId = ? WHERE name = ? AND domain = ?', [ ownerId, name, domain ], function (error, result) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.affectedRows === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

        callback(null);
    });
}

function addList(name, domain, members, membersOnly, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert(Array.isArray(members));
    assert.strictEqual(typeof membersOnly, 'boolean');
    assert.strictEqual(typeof callback, 'function');

    database.query('INSERT INTO mailboxes (name, type, domain, ownerId, membersJson, membersOnly) VALUES (?, ?, ?, ?, ?, ?)',
        [ name, exports.TYPE_LIST, domain, 'admin', JSON.stringify(members), membersOnly ], function (error) {
            if (error && error.code === 'ER_DUP_ENTRY') return callback(new BoxError(BoxError.ALREADY_EXISTS, 'mailbox already exists'));
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

            callback(null);
        });
}

function updateList(name, domain, members, membersOnly, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert(Array.isArray(members));
    assert.strictEqual(typeof membersOnly, 'boolean');
    assert.strictEqual(typeof callback, 'function');

    database.query('UPDATE mailboxes SET membersJson = ?, membersOnly = ? WHERE name = ? AND domain = ?',
        [ JSON.stringify(members), membersOnly, name, domain ], function (error, result) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
            if (result.affectedRows === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

            callback(null);
        });
}

function clear(callback) {
    assert.strictEqual(typeof callback, 'function');

    database.query('TRUNCATE TABLE mailboxes', [], function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        callback(null);
    });
}

function del(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    // deletes aliases as well
    database.query('DELETE FROM mailboxes WHERE ((name=? AND domain=?) OR (aliasName = ? AND aliasDomain=?))', [ name, domain, name, domain ], function (error, result) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.affectedRows === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

        callback(null);
    });
}

function delByDomain(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('DELETE FROM mailboxes WHERE domain = ?', [ domain ], function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function delByOwnerId(id, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('DELETE FROM mailboxes WHERE ownerId=?', [ id ], function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function updateName(oldName, oldDomain, newName, newDomain, callback) {
    assert.strictEqual(typeof oldName, 'string');
    assert.strictEqual(typeof oldDomain, 'string');
    assert.strictEqual(typeof newName, 'string');
    assert.strictEqual(typeof newDomain, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip if no changes
    if (oldName === newName && oldDomain === newDomain) return callback(null);

    database.query('UPDATE mailboxes SET name=?, domain=? WHERE name=? AND domain = ?', [ newName, newDomain, oldName, oldDomain ], function (error, result) {
        if (error && error.code === 'ER_DUP_ENTRY') return callback(new BoxError(BoxError.ALREADY_EXISTS, 'mailbox already exists'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.affectedRows !== 1) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

        callback(null);
    });
}

function get(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE name = ? AND domain = ?',
        [ name, domain ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
            if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

            callback(null, postProcess(results[0]));
        });
}

function getMailbox(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE name = ? AND type = ? AND domain = ?',
        [ name, exports.TYPE_MAILBOX, domain ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
            if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

            callback(null, postProcess(results[0]));
        });
}

function getMailboxCount(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT COUNT(*) AS total FROM mailboxes WHERE type = ? AND domain = ?', [ exports.TYPE_MAILBOX, domain ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null, results[0].total);
    });
}

function listMailboxes(domain, search, page, perPage, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert(typeof search === 'string' || search === null);
    assert.strictEqual(typeof page, 'number');
    assert.strictEqual(typeof perPage, 'number');
    assert.strictEqual(typeof callback, 'function');

    let query = `SELECT ${MAILBOX_FIELDS} FROM mailboxes WHERE type = ? AND domain = ?`;
    if (search) query += ' AND (name LIKE ' + mysql.escape('%' + search + '%') + ')';
    query += 'ORDER BY name LIMIT ?,?';

    database.query(query, [ exports.TYPE_MAILBOX, domain, (page-1)*perPage, perPage ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        results.forEach(function (result) { postProcess(result); });

        callback(null, results);
    });
}

function listAllMailboxes(page, perPage, callback) {
    assert.strictEqual(typeof page, 'number');
    assert.strictEqual(typeof perPage, 'number');
    assert.strictEqual(typeof callback, 'function');

    database.query(`SELECT ${MAILBOX_FIELDS} FROM mailboxes WHERE type = ? ORDER BY name LIMIT ?,?`,
        [ exports.TYPE_MAILBOX, (page-1)*perPage, perPage ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

            results.forEach(function (result) { postProcess(result); });

            callback(null, results);
        });
}

function getLists(domain, search, page, perPage, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert(typeof search === 'string' || search === null);
    assert.strictEqual(typeof page, 'number');
    assert.strictEqual(typeof perPage, 'number');
    assert.strictEqual(typeof callback, 'function');

    let query = `SELECT ${MAILBOX_FIELDS} FROM mailboxes WHERE type = ? AND domain = ?`;
    if (search) query += ' AND (name LIKE ' + mysql.escape('%' + search + '%') + ' OR membersJson LIKE ' + mysql.escape('%' + search + '%') + ')';

    query += 'ORDER BY name LIMIT ?,?';

    database.query(query, [ exports.TYPE_LIST, domain, (page-1)*perPage, perPage ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        results.forEach(function (result) { postProcess(result); });

        callback(null, results);
    });
}

function getList(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE type = ? AND name = ? AND domain = ?',
        [ exports.TYPE_LIST, name, domain ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
            if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

            callback(null, postProcess(results[0]));
        });
}

function getByOwnerId(ownerId, callback) {
    assert.strictEqual(typeof ownerId, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE ownerId = ? ORDER BY name', [ ownerId ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

        results.forEach(function (result) { postProcess(result); });

        callback(null, results);
    });
}

function setAliasesForName(name, domain, aliases, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert(util.isArray(aliases));
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE name = ? AND domain = ?', [ name, domain ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

        var queries = [];
        // clear existing aliases
        queries.push({ query: 'DELETE FROM mailboxes WHERE aliasName = ? AND aliasDomain = ? AND type = ?', args: [ name, domain, exports.TYPE_ALIAS ] });
        aliases.forEach(function (alias) {
            queries.push({ query: 'INSERT INTO mailboxes (name, domain, type, aliasName, aliasDomain, ownerId) VALUES (?, ?, ?, ?, ?, ?)',
                args: [ alias.name, alias.domain, exports.TYPE_ALIAS, name, domain, results[0].ownerId ] });
        });

        database.transaction(queries, function (error) {
            if (error && error.code === 'ER_DUP_ENTRY' && error.message.indexOf('mailboxes_name_domain_unique_index') !== -1) {
                var aliasMatch = error.message.match(new RegExp(`^ER_DUP_ENTRY: Duplicate entry '(.*)-${domain}' for key 'mailboxes_name_domain_unique_index'$`));
                if (!aliasMatch) return callback(new BoxError(BoxError.ALREADY_EXISTS, error));

                return callback(new BoxError(BoxError.ALREADY_EXISTS, `Mailbox, mailinglist or alias for ${aliasMatch[1]} already exists`));
            }

            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

            callback(null);
        });
    });
}

function getAliasesForName(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT name, domain FROM mailboxes WHERE type = ? AND aliasName = ? AND aliasDomain = ? ORDER BY name',
        [ exports.TYPE_ALIAS, name, domain ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

            callback(null, results);
        });
}

function getAlias(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILBOX_FIELDS + ' FROM mailboxes WHERE name = ? AND type = ? AND domain = ?',
        [ name, exports.TYPE_ALIAS, domain ], function (error, results) {
            if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
            if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mailbox not found'));

            results.forEach(function (result) { postProcess(result); });

            callback(null, results[0]);
        });
}
