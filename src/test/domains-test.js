/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var async = require('async'),
    database = require('../database.js'),
    domains = require('../domains.js'),
    expect = require('expect.js'),
    settings = require('../settings.js');

describe('Domains', function () {
    before(function (done) {
        async.series([
            database.initialize,
            database._clear,
            settings.setAdminLocation.bind(null, 'example.com', 'my.example.com')
        ], done);
    });

    after(function (done) {
        async.series([
            database._clear,
            database.uninitialize
        ], done);
    });

    describe('validateHostname', function () {
        const domain = {
            domain: 'example.com',
            zoneName: 'example.com',
            config: {}
        };

        it('does not allow admin subdomain', function () {
            expect(domains.validateHostname('my', domain)).to.be.an(Error);
        });

        it('cannot have >63 length subdomains', function () {
            var s = Array(64).fill('s').join('');
            expect(domains.validateHostname(s, domain)).to.be.an(Error);
            domain.zoneName = `dev.${s}.example.com`;
            expect(domains.validateHostname(`dev.${s}`, domain)).to.be.an(Error);
        });

        it('allows only alphanumerics and hypen', function () {
            expect(domains.validateHostname('#2r',   domain)).to.be.an(Error);
            expect(domains.validateHostname('a%b',   domain)).to.be.an(Error);
            expect(domains.validateHostname('ab_',   domain)).to.be.an(Error);
            expect(domains.validateHostname('ab.',   domain)).to.be.an(Error);
            expect(domains.validateHostname('ab..c', domain)).to.be.an(Error);
            expect(domains.validateHostname('.ab',   domain)).to.be.an(Error);
            expect(domains.validateHostname('-ab',   domain)).to.be.an(Error);
            expect(domains.validateHostname('ab-',   domain)).to.be.an(Error);
        });

        it('total length cannot exceed 255', function () {
            var s = '';
            for (var i = 0; i < (255 - 'example.com'.length); i++) s += 's';

            expect(domains.validateHostname(s, domain)).to.be.an(Error);
        });

        it('allow valid domains', function () {
            expect(domains.validateHostname('a',        domain)).to.be(null);
            expect(domains.validateHostname('a0-x',     domain)).to.be(null);
            expect(domains.validateHostname('a0.x',     domain)).to.be(null);
            expect(domains.validateHostname('a0.x.y',   domain)).to.be(null);
            expect(domains.validateHostname('01',       domain)).to.be(null);
        });
    });

    describe('getName', function () {
        it('works with zoneName==domain', function () {
            const domain = {
                domain: 'example.com',
                zoneName: 'example.com',
                config: {}
            };

            expect(domains.getName(domain, '', 'A')).to.be('');
            expect(domains.getName(domain, 'www', 'A')).to.be('www');
            expect(domains.getName(domain, 'www.dev', 'A')).to.be('www.dev');

            expect(domains.getName(domain, '', 'MX')).to.be('');

            expect(domains.getName(domain, '', 'TXT')).to.be('');
            expect(domains.getName(domain, 'www', 'TXT')).to.be('www');
            expect(domains.getName(domain, 'www.dev', 'TXT')).to.be('www.dev');
        });

        it('works when zoneName!=domain', function () {
            const domain = {
                domain: 'dev.example.com',
                zoneName: 'example.com',
                config: {}
            };

            expect(domains.getName(domain, '', 'A')).to.be('dev');
            expect(domains.getName(domain, 'www', 'A')).to.be('www.dev');
            expect(domains.getName(domain, 'www.dev', 'A')).to.be('www.dev.dev');

            expect(domains.getName(domain, '', 'MX')).to.be('dev');

            expect(domains.getName(domain, '', 'TXT')).to.be('dev');
            expect(domains.getName(domain, 'www', 'TXT')).to.be('www.dev');
            expect(domains.getName(domain, 'www.dev', 'TXT')).to.be('www.dev.dev');
        });
    });
});
