/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var async = require('async'),
    BoxError = require('../boxerror.js'),
    database = require('../database.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    groupdb = require('../groupdb.js'),
    groups = require('../groups.js'),
    mailboxdb = require('../mailboxdb.js'),
    maildb = require('../maildb.js'),
    mailer = require('../mailer.js'),
    paths = require('../paths.js'),
    provision = require('../provision.js'),
    userdb = require('../userdb.js'),
    users = require('../users.js'),
    _ = require('underscore');

var USERNAME = 'noBody';
var USERNAME_NEW = 'noBodyNew';
var EMAIL = 'else@no.body';
var EMAIL_NEW = 'noBodyNew@no.body';
var PASSWORD = 'sTrOnG#$34134';
var NEW_PASSWORD = 'oTHER@#$235';
var DISPLAY_NAME = 'Nobody cares';
var DISPLAY_NAME_NEW = 'Somone cares';
var userObject = null;
var NON_ADMIN_GROUP = 'members';
var AUDIT_SOURCE = { ip: '1.2.3.4', userId: 'someuserid' };

const DOMAIN_0 = {
    domain: 'example.com',
    zoneName: 'example.com',
    provider: 'manual',
    config: {},
    fallbackCertificate: null,
    tlsConfig: { provider: 'fallback' }
};

function cleanupUsers(done) {
    mailer._mailQueue = [];

    async.series([
        groupdb._clear,
        userdb._clear,
        mailboxdb._clear,
    ], done);
}

function createOwner(done) {
    users.createOwner(USERNAME, PASSWORD, EMAIL, DISPLAY_NAME, AUDIT_SOURCE, function (error, result) {
        expect(error).to.not.be.ok();
        expect(result).to.be.ok();

        userObject = result;

        done(null, userObject);
    });
}

function setup(done) {
    mailer._mailQueue = [];

    async.series([
        database.initialize,
        database._clear,
        provision.setup.bind(null, DOMAIN_0, { provider: 'generic' }, AUDIT_SOURCE),
    ], done);
}

function cleanup(done) {
    mailer._mailQueue = [];

    async.series([
        database._clear,
        database.uninitialize
    ], done);
}

function checkMails(number, options, callback) {
    if (typeof options === 'function') {
        callback = options;
        options = null;
    }

    // mails are enqueued async
    setTimeout(function () {
        expect(mailer._mailQueue.length).to.equal(number);

        if (options && options.sentTo) expect(mailer._mailQueue.some(function (mail) { return mail.to === options.sentTo; }));

        mailer._mailQueue = [];

        callback();
    }, 500);
}

describe('User', function () {
    before(setup);
    after(cleanup);

    describe('create', function() {
        before(cleanupUsers);
        after(cleanupUsers);

        it('fails due to short password', function (done) {
            users.create(USERNAME, 'Fo$%23', EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('fails due to reserved username', function (done) {
            users.create('admin', PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('fails due to invalid username', function (done) {
            users.create('moo+daemon', PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('fails due to short username', function (done) {
            users.create('', PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('fails due to long username', function (done) {
            users.create(new Array(257).fill('Z').join(''), PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('fails due to reserved app pattern', function (done) {
            users.create('maybe.app', PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('succeeds', function (done) {
            users.createOwner(USERNAME, PASSWORD, EMAIL, DISPLAY_NAME, AUDIT_SOURCE, function (error, result) {
                expect(error).not.to.be.ok();
                expect(result).to.be.ok();
                expect(result.username).to.equal(USERNAME.toLowerCase());
                expect(result.email).to.equal(EMAIL.toLowerCase());
                expect(result.fallbackEmail).to.equal(EMAIL.toLowerCase());

                done();
            });
        });

        it('fails because user exists', function (done) {
            users.create(USERNAME, PASSWORD, EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).not.to.be.ok();
                expect(error.reason).to.equal(BoxError.ALREADY_EXISTS);

                done();
            });
        });

        it('fails because password is empty', function (done) {
            users.create(USERNAME, '', EMAIL, DISPLAY_NAME, { }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.be.ok();
                expect(result).not.to.be.ok();
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });
    });

    describe('getOwner', function() {
        before(cleanupUsers);
        after(cleanupUsers);

        it('fails because there is no owner', function (done) {
            users.getOwner(function (error) {
                expect(error.reason).to.be(BoxError.NOT_FOUND);
                done();
            });
        });

        it('succeeds', function (done) {
            createOwner(function (error) {
                if (error) return done(error);

                users.getOwner(function (error, owner) {
                    expect(error).to.be(null);
                    expect(owner.email).to.be(EMAIL.toLowerCase());
                    done();
                });
            });
        });
    });

    describe('verify', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to non existing user', function (done) {
            users.verify('somerandomid', PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.NOT_FOUND);

                done();
            });
        });

        it('fails due to empty password', function (done) {
            users.verify(userObject.id, '', users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('fails due to wrong password', function (done) {
            users.verify(userObject.id, PASSWORD+PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('succeeds', function (done) {
            users.verify(userObject.id, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });
        });

        it('fails for ghost if not enabled', function (done) {
            users.verify(userObject.id, 'foobar', users.AP_WEBADMIN, function (error) {
                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);
                done();
            });
        });

        it('fails for ghost with wrong password', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';
            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verify(userObject.id, 'foobar', users.AP_WEBADMIN, function (error) {
                fs.unlinkSync(paths.GHOST_USER_FILE);

                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);
                done();
            });
        });

        it('succeeds for ghost', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';
            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verify(userObject.id, 'testpassword', users.AP_WEBADMIN, function (error, result) {
                if (fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file exists after verification'));

                expect(error).to.equal(null);
                expect(result.id).to.equal(userObject.id);
                expect(result.username).to.equal(userObject.username);
                expect(result.email).to.equal(userObject.email);
                expect(result.displayName).to.equal(userObject.displayName);

                done();
            });
        });

        it('succeeds for normal user password when ghost file exists', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';
            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verify(userObject.id, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                if (!fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file went way without verification'));

                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });

        });
    });

    describe('verifyWithUsername', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to non existing username', function (done) {
            users.verifyWithUsername(USERNAME+USERNAME, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.NOT_FOUND);

                done();
            });
        });

        it('fails due to empty password', function (done) {
            users.verifyWithUsername(USERNAME, '', users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('fails due to wrong password', function (done) {
            users.verifyWithUsername(USERNAME, PASSWORD+PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('succeeds', function (done) {
            users.verifyWithUsername(USERNAME, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });
        });

        it('succeeds for different username case', function (done) {
            users.verifyWithUsername(USERNAME.toUpperCase(), PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });
        });

        it('fails for ghost with wrong password', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';

            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verifyWithUsername(USERNAME, 'foobar', users.AP_WEBADMIN, function (error) {
                if (!fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file went way without verification'));
                fs.unlinkSync(paths.GHOST_USER_FILE);

                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);
                done();
            });
        });

        it('succeeds for ghost', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';

            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verifyWithUsername(USERNAME, 'testpassword', users.AP_WEBADMIN, function (error, result) {
                if (fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file still around!'));

                expect(error).to.equal(null);
                expect(result.id).to.equal(userObject.id);
                expect(result.username).to.equal(userObject.username);
                expect(result.email).to.equal(userObject.email);
                expect(result.displayName).to.equal(userObject.displayName);

                done();
            });
        });
    });

    describe('verifyWithEmail', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to non existing user', function (done) {
            users.verifyWithEmail(EMAIL+EMAIL, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.NOT_FOUND);

                done();
            });
        });

        it('fails due to empty password', function (done) {
            users.verifyWithEmail(EMAIL, '', users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('fails due to wrong password', function (done) {
            users.verifyWithEmail(EMAIL, PASSWORD+PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('succeeds', function (done) {
            users.verifyWithEmail(EMAIL, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });
        });

        it('succeeds for different email case', function (done) {
            users.verifyWithEmail(EMAIL.toUpperCase(), PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                done();
            });
        });

        it('fails for ghost with wrong password', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';

            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verifyWithEmail(EMAIL, 'foobar', users.AP_WEBADMIN, function (error) {
                if (!fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file not found after failed login!'));
                fs.unlinkSync(paths.GHOST_USER_FILE);

                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);
                done();
            });
        });

        it('succeeds for ghost', function (done) {
            var ghost = { };
            ghost[userObject.username] = 'testpassword';

            fs.writeFileSync(paths.GHOST_USER_FILE, JSON.stringify(ghost), 'utf8');

            users.verifyWithEmail(EMAIL, 'testpassword', users.AP_WEBADMIN, function (error, result) {
                if (fs.existsSync(paths.GHOST_USER_FILE)) return done(new Error('Ghost file still around!'));

                expect(error).to.equal(null);
                expect(result.id).to.equal(userObject.id);
                expect(result.username).to.equal(userObject.username);
                expect(result.email).to.equal(userObject.email);
                expect(result.displayName).to.equal(userObject.displayName);

                done();
            });
        });
    });

    describe('active', function () {
        before(createOwner);
        after(cleanupUsers);

        it('verify fails for inactive user', function (done) {
            users.update(userObject, { active: false }, AUDIT_SOURCE, function (error) {
                expect(error).to.not.be.ok();

                users.verify(userObject.id, PASSWORD, users.AP_WEBADMIN, function (error) {
                    expect(error).to.be.ok();
                    expect(error.reason).to.equal(BoxError.NOT_FOUND);

                    done();
                });
            });
        });

        it('verify succeeds for inactive user', function (done) {
            users.update(userObject, { active: true }, AUDIT_SOURCE, function (error) {
                expect(error).to.not.be.ok();

                users.verify(userObject.id, PASSWORD, users.AP_WEBADMIN, function (error) {
                    expect(error).to.not.be.ok();

                    done();
                });
            });
        });
    });

    describe('appPasswords', function () {
        before(createOwner);
        after(cleanupUsers);
        let pwd;

        it('can add app password', function (done) {
            users.addAppPassword(userObject.id, 'appid', 'rpi', function (error, result) {
                expect(error).to.be(null);
                pwd = result;
                done();
            });
        });

        it('can get app passwords', function (done) {
            users.getAppPasswords(userObject.id, function (error, result) {
                expect(error).to.be(null);
                expect(result.length).to.be(1);
                expect(result[0].name).to.be('rpi');
                expect(result[0].identifier).to.be('appid');
                expect(result[0].hashedPassword).to.be(undefined);
                done();
            });
        });

        it('can get app password', function (done) {
            users.getAppPassword(pwd.id, function (error, result) {
                expect(error).to.be(null);
                expect(result.name).to.be('rpi');
                expect(result.identifier).to.be('appid');
                expect(result.hashedPassword).to.be(undefined);
                done();
            });
        });

        it('can verify app password', function (done) {
            users.verify(userObject.id, pwd.password, 'appid', function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();
                expect(result.appPassword).to.be(true);

                done();
            });
        });

        it('can verify non-app password', function (done) {
            users.verify(userObject.id, PASSWORD, 'appid', function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();
                expect(result.appPassword).to.be(undefined);

                done();
            });
        });

        it('cannot verify bad password', function (done) {
            users.verify(userObject.id, 'bad', 'appid', function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.be(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('cannot verify password for another app', function (done) {
            users.verify(userObject.id, pwd.password, 'appid2', function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.be(BoxError.INVALID_CREDENTIALS);

                done();
            });
        });

        it('can del app password', function (done) {
            users.delAppPassword(pwd.id, function (error) {
                if (error) return done(error);

                // cannot verify anymore
                users.verify(userObject.id, pwd.password, 'appid', function (error) {
                    expect(error).to.be.ok();

                    done();
                });
            });
        });
    });

    describe('retrieving', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to non existing user', function (done) {
            users.get('some non existing username', function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();

                done();
            });
        });

        it('succeeds', function (done) {
            users.get(userObject.id, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();
                expect(result.id).to.equal(userObject.id);
                expect(result.email).to.equal(EMAIL.toLowerCase());
                expect(result.fallbackEmail).to.equal(EMAIL.toLowerCase());
                expect(result.username).to.equal(USERNAME.toLowerCase());
                expect(result.displayName).to.equal(DISPLAY_NAME);

                done();
            });
        });

        it('succeeds with email enabled', function (done) {
            // use maildb to not trigger further events
            maildb.update(DOMAIN_0.domain, { enabled: true }, function (error) {
                expect(error).not.to.be.ok();

                users.get(userObject.id, function (error, result) {
                    expect(error).to.not.be.ok();
                    expect(result).to.be.ok();
                    expect(result.id).to.equal(userObject.id);
                    expect(result.email).to.equal(EMAIL.toLowerCase());
                    expect(result.fallbackEmail).to.equal(EMAIL.toLowerCase());
                    expect(result.username).to.equal(USERNAME.toLowerCase());
                    expect(result.displayName).to.equal(DISPLAY_NAME);

                    maildb.update(DOMAIN_0.domain, { enabled: false }, done);
                });
            });
        });
    });

    describe('update', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to unknown userid', function (done) {
            var data = { username: USERNAME_NEW, email: EMAIL_NEW, displayName: DISPLAY_NAME_NEW };
            users.update(_.extend({}, userObject, { id: 'random' }), data, AUDIT_SOURCE, function (error) {
                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.NOT_FOUND);

                done();
            });
        });

        it('fails due to invalid email', function (done) {
            var data = { username: USERNAME_NEW, email: 'brokenemailaddress', displayName: DISPLAY_NAME_NEW };
            users.update(userObject, data, AUDIT_SOURCE, function (error) {
                expect(error).to.be.a(BoxError);
                expect(error.reason).to.equal(BoxError.BAD_FIELD);

                done();
            });
        });

        it('succeeds', function (done) {
            var data = { username: USERNAME_NEW, email: EMAIL_NEW, displayName: DISPLAY_NAME_NEW };

            users.update(userObject, data, AUDIT_SOURCE, function (error) {
                expect(error).to.not.be.ok();

                users.get(userObject.id, function (error, result) {
                    expect(error).to.not.be.ok();
                    expect(result).to.be.ok();
                    expect(result.email).to.equal(EMAIL_NEW.toLowerCase());
                    expect(result.username).to.equal(USERNAME_NEW.toLowerCase());
                    expect(result.displayName).to.equal(DISPLAY_NAME_NEW);

                    done();
                });
            });
        });

        it('succeeds with same data', function (done) {
            var data = { username: USERNAME_NEW, email: EMAIL_NEW, displayName: DISPLAY_NAME_NEW };

            users.update(userObject, data, AUDIT_SOURCE, function (error) {
                expect(error).to.not.be.ok();

                users.get(userObject.id, function (error, result) {
                    expect(error).to.not.be.ok();
                    expect(result).to.be.ok();
                    expect(result.email).to.equal(EMAIL_NEW.toLowerCase());
                    expect(result.username).to.equal(USERNAME_NEW.toLowerCase());
                    expect(result.displayName).to.equal(DISPLAY_NAME_NEW);

                    done();
                });
            });
        });
    });

    describe('admin change mail triggers', function () {
        let auditSource;

        before(function (done) {
            createOwner(function (error, owner) {
                expect(error).to.not.be.ok();

                auditSource = _.extend({}, AUDIT_SOURCE, { userId: owner.id });

                groups.create(NON_ADMIN_GROUP, '', done);
            });
        });

        after(cleanupUsers);

        var user1 = {
            username: 'seconduser',
            password: 'ASDFkljsf#$^%2354',
            email: 'some@thi.ng',
            role: users.ROLE_ADMIN
        };

        it('make second user admin does not send mail to action performer', function (done) {
            users.create(user1.username, user1.password, user1.email, DISPLAY_NAME, { }, auditSource, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                user1.id = result.id;

                users.update(user1, { role: users.ROLE_ADMIN }, AUDIT_SOURCE, function (error) {
                    expect(error).to.not.be.ok();

                    user1.role = users.ROLE_ADMIN;
                    // no emails should be sent out anymore, since the user performing the action does not get a notification anymore
                    checkMails(0, done);
                });
            });
        });

        it('succeeds to remove admin flag does not send mail to action performer', function (done) {
            users.update(user1, { role: users.ROLE_USER }, auditSource, function (error) {
                expect(error).to.eql(null);

                user1.role = users.ROLE_USER;
                // no emails should be sent out anymore, since the user performing the action does not get a notification anymore
                checkMails(0, done);
            });
        });

        it('make second user admin does send mail to other admins', function (done) {
            users.update(user1, { role: users.ROLE_ADMIN }, { ip: '1.2.3.4', userId: 'someuserid' }, function (error) {
                expect(error).to.not.be.ok();

                user1.role = users.ROLE_ADMIN;
                checkMails(1, done);
            });
        });

        it('succeeds to remove admin flag does send mail to other admins', function (done) {
            users.update(user1, { role: users.ROLE_USER }, { ip: '1.2.3.4', userId: 'someuserid' }, function (error) {
                expect(error).to.eql(null);

                user1.role = users.ROLE_USER;
                checkMails(1, done);
            });
        });
    });

    describe('get admins', function () {
        before(createOwner);
        after(cleanupUsers);

        it('succeeds for one admins', function (done) {
            users.getAdmins(function (error, admins) {
                expect(error).to.eql(null);
                expect(admins.length).to.equal(1);
                expect(admins[0].username).to.equal(USERNAME.toLowerCase());
                done();
            });
        });

        it('succeeds for two admins', function (done) {
            var user1 = {
                username: 'seconduser',
                password: 'Adfasdkjf#$%43',
                email: 'some@thi.ng',
                role: users.ROLE_ADMIN
            };

            users.create(user1.username, user1.password, user1.email, DISPLAY_NAME, { role: users.ROLE_ADMIN }, AUDIT_SOURCE, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();

                user1.id = result.id;

                users.update(user1, { role: users.ROLE_ADMIN }, AUDIT_SOURCE, function (error) {
                    expect(error).to.eql(null);

                    users.getAdmins(function (error, admins) {
                        expect(error).to.eql(null);
                        expect(admins.length).to.equal(2);
                        expect(admins[0].username).to.equal(USERNAME.toLowerCase());
                        expect(admins[1].username).to.equal(user1.username.toLowerCase());

                        done();
                    });
                });
            });
        });
    });

    describe('activated', function () {
        after(cleanupUsers);

        it('succeeds with no users', function (done) {
            users.isActivated(function (error, activated) {
                expect(error).to.not.be.ok();
                expect(activated).to.be(false);
                done();
            });
        });

        it('create users', function (done) {
            createOwner(done);
        });

        it('succeeds with users', function (done) {
            users.isActivated(function (error, activated) {
                expect(error).to.not.be.ok();
                expect(activated).to.be(true);
                done();
            });
        });
    });

    describe('set password', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to unknown user', function (done) {
            users.setPassword(_.extend({}, userObject, { id: 'doesnotexist' }), NEW_PASSWORD, function (error) {
                expect(error).to.be.ok();
                done();
            });
        });

        it('fails due to empty password', function (done) {
            users.setPassword(userObject, '', function (error) {
                expect(error).to.be.ok();
                done();
            });
        });

        it('fails due to invalid password', function (done) {
            users.setPassword(userObject, 'foobar', function (error) {
                expect(error).to.be.ok();
                done();
            });
        });

        it('succeeds', function (done) {
            users.setPassword(userObject, NEW_PASSWORD, function (error) {
                expect(error).to.not.be.ok();
                done();
            });
        });

        it('actually changed the password (unable to login with old pasword)', function (done) {
            users.verify(userObject.id, PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.be.ok();
                expect(result).to.not.be.ok();
                expect(error.reason).to.equal(BoxError.INVALID_CREDENTIALS);
                done();
            });
        });

        it('actually changed the password (login with new password)', function (done) {
            users.verify(userObject.id, NEW_PASSWORD, users.AP_WEBADMIN, function (error, result) {
                expect(error).to.not.be.ok();
                expect(result).to.be.ok();
                done();
            });
        });
    });

    describe('sendPasswordResetByIdentifier', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails due to unkown email', function (done) {
            users.sendPasswordResetByIdentifier('unknown@mail.com', function (error) {
                expect(error).to.be.an(BoxError);
                expect(error.reason).to.eql(BoxError.NOT_FOUND);
                done();
            });
        });

        it('fails due to unkown username', function (done) {
            users.sendPasswordResetByIdentifier('unknown', function (error) {
                expect(error).to.be.an(BoxError);
                expect(error.reason).to.eql(BoxError.NOT_FOUND);
                done();
            });
        });

        it('succeeds with email', function (done) {
            users.sendPasswordResetByIdentifier(EMAIL, function (error) {
                expect(error).to.not.be.ok();
                checkMails(1, done);
            });
        });

        it('succeeds with username', function (done) {
            users.sendPasswordResetByIdentifier(USERNAME, function (error) {
                expect(error).to.not.be.ok();
                checkMails(1, done);
            });
        });
    });

    describe('invite', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails as expected', function (done) {
            users.sendInvite(userObject, { }, function (error) {
                expect(error).to.be.ok(); // have to create resetToken first
                done();
            });
        });

        it('can create token', function (done) {
            users.createInvite(userObject, function (error, resetToken) {
                expect(error).to.be(null);
                expect(resetToken).to.be.ok();
                done();
            });
        });

        it('send invite', function (done) {
            users.sendInvite(userObject, { }, function (error) {
                expect(error).to.be(null);
                checkMails(1, done);
            });
        });
    });

    describe('remove', function () {
        before(createOwner);
        after(cleanupUsers);

        it('fails for unknown user', function (done) {
            users.remove(_.extend({}, userObject, { id: 'unknown' }), AUDIT_SOURCE, function (error) {
                expect(error.reason).to.be(BoxError.NOT_FOUND);
                done();
            });
        });

        it('can remove valid user', function (done) {
            users.remove(userObject, AUDIT_SOURCE, function (error) {
                expect(!error).to.be.ok();
                done();
            });
        });

        it('can re-create user after user was removed', createOwner);
    });
});
