/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var async = require('async'),
    database = require('../database.js'),
    expect = require('expect.js'),
    system = require('../system.js');

function setup(done) {
    async.series([
        database.initialize,
        database._clear
    ], done);
}

function cleanup(done) {
    async.series([
        database._clear,
        database.uninitialize
    ], done);
}

describe('System', function () {
    before(setup);
    after(cleanup);

    it('can get disks', function (done) {
        system.getDisks(function (error, disks) {
            expect(!error).to.be.ok();
            expect(disks).to.be.ok();
            done();
        });
    });

    it('can check for disk space', function (done) {
        system.checkDiskSpace(function (error) {
            expect(!error).to.be.ok();
            done();
        });
    });

    it('can get memory', function (done) {
        system.getMemory(function (error, memory) {
            expect(!error).to.be.ok();

            expect(memory.memory).to.be.a('number');
            expect(memory.swap).to.be.a('number');

            done();
        });
    });
});

