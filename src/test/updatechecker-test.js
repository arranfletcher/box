/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var appdb = require('../appdb.js'),
    apps = require('../apps.js'),
    async = require('async'),
    constants = require('../constants.js'),
    cron = require('../cron.js'),
    database = require('../database.js'),
    domains = require('../domains.js'),
    expect = require('expect.js'),
    mailer = require('../mailer.js'),
    nock = require('nock'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    semver = require('semver'),
    settings = require('../settings.js'),
    settingsdb = require('../settingsdb.js'),
    updatechecker = require('../updatechecker.js'),
    users = require('../users.js');

// owner
var USER_0 = {
    username: 'username0',
    password: 'Username0pass?1234',
    email: 'user0@email.com',
    displayName: 'User 0',
    source: '',
    permissions: null
};

const ADMIN_DOMAIN = 'updatechecker-test.example.com';

const DOMAIN_0 = {
    domain: 'example.com',
    zoneName: 'example.com',
    config: {},
    provider: 'manual',
    fallbackCertificate: null,
    tlsConfig: { provider: 'fallback' }
};

var AUDIT_SOURCE = {
    ip: '1.2.3.4'
};

const UPDATE_VERSION = semver.inc(constants.VERSION, 'major');

function checkMails(number, done) {
    // mails are enqueued async
    setTimeout(function () {
        expect(mailer._mailQueue.length).to.equal(number);
        mailer._mailQueue = [];
        done();
    }, 500);
}

function cleanup(done) {
    mailer._mailQueue = [];
    safe.fs.unlinkSync(paths.UPDATE_CHECKER_FILE);

    async.series([
        cron.stopJobs,
        database._clear,
        database.uninitialize
    ], done);
}

describe('updatechecker - box - manual (email)', function () {
    before(function (done) {
        safe.fs.unlinkSync(paths.UPDATE_CHECKER_FILE);

        mailer._mailQueue = [];

        async.series([
            database.initialize,
            database._clear,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            settings.setAdminLocation.bind(null, ADMIN_DOMAIN, 'my.' + ADMIN_DOMAIN),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            settings.setAutoupdatePattern.bind(null, constants.AUTOUPDATE_PATTERN_NEVER),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('no updates', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/boxupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', automatic: false })
            .reply(204, { } );

        updatechecker.checkForUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().box).to.be(null);
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });

    it('new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/boxupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', automatic: false })
            .reply(200, { version: UPDATE_VERSION, changelog: [''], sourceTarballUrl: 'box.tar.gz', sourceTarballSigUrl: 'box.tar.gz.sig', boxVersionsUrl: 'box.versions', boxVersionsSigUrl: 'box.versions.sig' } );

        updatechecker.checkForUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().box.version).to.be(UPDATE_VERSION);
            expect(updatechecker.getUpdateInfo().box.sourceTarballUrl).to.be('box.tar.gz');
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });

    it('bad response offers nothing', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/boxupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', automatic: false })
            .reply(404, { version: '2.0.0-pre.0', changelog: [''], sourceTarballUrl: 'box-pre.tar.gz' } );

        updatechecker.checkForUpdates({ automatic: false }, function (error) {
            expect(error).to.be.ok();
            expect(updatechecker.getUpdateInfo().box).to.be(null);
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });
});

describe('updatechecker - box - automatic (no email)', function () {
    before(function (done) {
        mailer._mailQueue = [];

        async.series([
            database.initialize,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/boxupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', automatic: false })
            .reply(200, { version: UPDATE_VERSION, changelog: [''], sourceTarballUrl: 'box.tar.gz', sourceTarballSigUrl: 'box.tar.gz.sig', boxVersionsUrl: 'box.versions', boxVersionsSigUrl: 'box.versions.sig' } );

        updatechecker.checkForUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().box.version).to.be(UPDATE_VERSION);
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });
});

describe('updatechecker - box - automatic free (email)', function () {
    before(function (done) {
        mailer._mailQueue = [];

        async.series([
            database.initialize,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/boxupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', automatic: false })
            .reply(200, { version: UPDATE_VERSION, changelog: [''], sourceTarballUrl: 'box.tar.gz', sourceTarballSigUrl: 'box.tar.gz.sig', boxVersionsUrl: 'box.versions', boxVersionsSigUrl: 'box.versions.sig' } );

        updatechecker.checkForUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().box.version).to.be(UPDATE_VERSION);
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });
});

describe('updatechecker - app - manual (email)', function () {
    var APP_0 = {
        id: 'appid-0',
        appStoreId: 'io.cloudron.app',
        installationState: apps.ISTATE_PENDING_INSTALL,
        error: null,
        runState: 'running',
        location: 'some-location-0',
        domain: DOMAIN_0.domain,
        manifest: {
            version: '1.0.0', dockerImage: 'docker/app0', healthCheckPath: '/', httpPort: 80, title: 'app0',
            tcpPorts: {
                PORT: {
                    description: 'this is a port that i expose',
                    containerPort: '1234'
                }
            }
        },
        httpPort: null,
        containerId: null,
        portBindings: { PORT: 5678 },
        healthy: null,
        accessRestriction: null,
        memoryLimit: 0,
        mailboxName: 'mail',
        mailboxDomain: DOMAIN_0.domain
    };

    before(function (done) {
        mailer._mailQueue = [];

        async.series([
            database.initialize,
            database._clear,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            appdb.add.bind(null, APP_0.id, APP_0.appStoreId, APP_0.manifest, APP_0.location, APP_0.domain, apps._translatePortBindings(APP_0.portBindings, APP_0.manifest), APP_0),
            settings.setAutoupdatePattern.bind(null, constants.AUTOUPDATE_PATTERN_NEVER),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('no updates', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/appupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', appId: APP_0.appStoreId, appVersion: APP_0.manifest.version, automatic: false })
            .reply(204, { } );

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({});
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });

    it('bad response', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/appupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', appId: APP_0.appStoreId, appVersion: APP_0.manifest.version, automatic: false })
            .reply(500, { update: { manifest: { version: '1.0.0', changelog: '* some changes' } } } );

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({});
            expect(scope.isDone()).to.be.ok();

            checkMails(0, done);
        });
    });

    it('offers new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/appupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', appId: APP_0.appStoreId, appVersion: APP_0.manifest.version, automatic: false })
            .reply(200, { manifest: { version: '2.0.0', changelog: '* some changes' } } );

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({ 'appid-0': { manifest: { version: '2.0.0', changelog: '* some changes' }, unstable: false } });
            expect(scope.isDone()).to.be.ok();

            checkMails(1, done);
        });
    });

    it('does not offer old version', function (done) {
        nock.cleanAll();

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({ });
            checkMails(0, done);
        });
    });
});

describe('updatechecker - app - automatic (no email)', function () {
    var APP_0 = {
        id: 'appid-0',
        appStoreId: 'io.cloudron.app',
        installationState: apps.ISTATE_PENDING_INSTALL,
        error: null,
        runState: 'running',
        location: 'some-location-0',
        domain: DOMAIN_0.domain,
        manifest: {
            version: '1.0.0', dockerImage: 'docker/app0', healthCheckPath: '/', httpPort: 80, title: 'app0',
            tcpPorts: {
                PORT: {
                    description: 'this is a port that i expose',
                    containerPort: '1234'
                }
            }
        },
        httpPort: null,
        containerId: null,
        portBindings: { PORT: 5678 },
        healthy: null,
        accessRestriction: null,
        memoryLimit: 0,
        mailboxName: 'mail',
        mailboxDomain: DOMAIN_0.domain
    };

    before(function (done) {
        mailer._mailQueue = [];

        async.series([
            database.initialize,
            database._clear,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            appdb.add.bind(null, APP_0.id, APP_0.appStoreId, APP_0.manifest, APP_0.location, APP_0.domain, apps._translatePortBindings(APP_0.portBindings, APP_0.manifest), APP_0),
            settings.setAutoupdatePattern.bind(null, '00 00 1,3,5,23 * * *'),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('offers new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/appupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', appId: APP_0.appStoreId, appVersion: APP_0.manifest.version, automatic: false })
            .reply(200, { manifest: { version: '2.0.0', changelog: 'c' } } );

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({ 'appid-0': { manifest: { version: '2.0.0', changelog: 'c' }, unstable: false } });
            expect(scope.isDone()).to.be.ok();

            checkMails(1, done);
        });
    });
});

describe('updatechecker - app - automatic free (email)', function () {
    var APP_0 = {
        id: 'appid-0',
        appStoreId: 'io.cloudron.app',
        installationState: apps.ISTATE_PENDING_INSTALL,
        error: null,
        runState: 'running',
        location: 'some-location-0',
        domain: DOMAIN_0.domain,
        manifest: {
            version: '1.0.0', dockerImage: 'docker/app0', healthCheckPath: '/', httpPort: 80, title: 'app0',
            tcpPorts: {
                PORT: {
                    description: 'this is a port that i expose',
                    containerPort: '1234'
                }
            }
        },
        httpPort: null,
        containerId: null,
        portBindings: { PORT: 5678 },
        healthy: null,
        accessRestriction: null,
        memoryLimit: 0,
        mailboxName: 'mail',
        mailboxDomain: DOMAIN_0.domain
    };

    before(function (done) {
        mailer._mailQueue = [];

        async.series([
            database.initialize,
            database._clear,
            settings._setApiServerOrigin.bind(null, 'http://localhost:4444'),
            cron.startJobs,
            domains.add.bind(null, DOMAIN_0.domain, DOMAIN_0, AUDIT_SOURCE),
            settings.setAdminLocation.bind(null, DOMAIN_0.domain, 'my.' + DOMAIN_0.domain),
            users.createOwner.bind(null, USER_0.username, USER_0.password, USER_0.email, USER_0.displayName, AUDIT_SOURCE),
            appdb.add.bind(null, APP_0.id, APP_0.appStoreId, APP_0.manifest, APP_0.location, APP_0.domain, apps._translatePortBindings(APP_0.portBindings, APP_0.manifest), APP_0),
            settings.setAutoupdatePattern.bind(null, '00 00 1,3,5,23 * * *'),
            settingsdb.set.bind(null, settings.CLOUDRON_TOKEN_KEY, 'atoken'),
        ], done);
    });

    after(cleanup);

    it('offers new version', function (done) {
        nock.cleanAll();

        var scope = nock('http://localhost:4444')
            .get('/api/v1/appupdate')
            .query({ boxVersion: constants.VERSION, accessToken: 'atoken', appId: APP_0.appStoreId, appVersion: APP_0.manifest.version, automatic: false })
            .reply(200, { manifest: { version: '2.0.0', changelog: 'c' } } );

        updatechecker._checkAppUpdates({ automatic: false }, function (error) {
            expect(!error).to.be.ok();
            expect(updatechecker.getUpdateInfo().apps).to.eql({ 'appid-0': { manifest: { version: '2.0.0', changelog: 'c' }, unstable: false } });
            expect(scope.isDone()).to.be.ok();

            checkMails(1, done);
        });
    });
});
