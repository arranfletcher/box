/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */
/* global beforeEach:false */

'use strict';

var async = require('async'),
    appstore = require('../appstore.js'),
    database = require('../database.js'),
    expect = require('expect.js'),
    nock = require('nock'),
    settings = require('../settings.js'),
    settingsdb = require('../settingsdb.js');

const ADMIN_DOMAIN = 'appstore-test.example.com';
const APPSTORE_TOKEN = 'appstoretoken';
const APP_ID = 'appid';
const APPSTORE_APP_ID = 'appstoreappid';
const MOCK_API_SERVER_ORIGIN = 'http://localhost:6060';

function setup(done) {
    nock.cleanAll();

    async.series([
        database.initialize,
        database._clear,
        settings._setApiServerOrigin.bind(null, MOCK_API_SERVER_ORIGIN),
        settings.setAdminLocation.bind(null, ADMIN_DOMAIN, 'my.' + ADMIN_DOMAIN),
        settings.initCache
    ], done);
}

function cleanup(done) {
    nock.cleanAll();

    async.series([
        database._clear,
        database.uninitialize
    ], done);
}

describe('Appstore', function () {
    before(setup);
    after(cleanup);

    beforeEach(nock.cleanAll);

    it('can set cloudron token', function (done) {
        settingsdb.set(settings.CLOUDRON_TOKEN_KEY, APPSTORE_TOKEN, done);
    });

    it('can purchase an app', function (done) {
        var scope1 = nock(MOCK_API_SERVER_ORIGIN)
            .post(`/api/v1/cloudronapps?accessToken=${APPSTORE_TOKEN}`, function () { return true; })
            .reply(201, {});

        appstore.purchaseApp({ appId: APP_ID, appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID }, function (error) {
            expect(error).to.not.be.ok();
            expect(scope1.isDone()).to.be.ok();

            done();
        });
    });

    it('unpurchase succeeds if app was never purchased', function (done) {
        var scope1 = nock(MOCK_API_SERVER_ORIGIN)
            .get(`/api/v1/cloudronapps/${APP_ID}?accessToken=${APPSTORE_TOKEN}`)
            .reply(404, {});

        var scope2 = nock(MOCK_API_SERVER_ORIGIN)
            .delete(`/api/v1/cloudronapps/${APP_ID}?accessToken=${APPSTORE_TOKEN}`, function () { return true; })
            .reply(204, {});

        appstore.unpurchaseApp(APP_ID, { appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID }, function (error) {
            expect(error).to.not.be.ok();
            expect(scope1.isDone()).to.be.ok();
            expect(scope2.isDone()).to.not.be.ok();

            done();
        });
    });

    it('can unpurchase an app', function (done) {
        var scope1 = nock(MOCK_API_SERVER_ORIGIN)
            .get(`/api/v1/cloudronapps/${APP_ID}?accessToken=${APPSTORE_TOKEN}`)
            .reply(200, {});

        var scope2 = nock(MOCK_API_SERVER_ORIGIN)
            .delete(`/api/v1/cloudronapps/${APP_ID}?accessToken=${APPSTORE_TOKEN}`, function () { return true; })
            .reply(204, {});

        appstore.unpurchaseApp(APP_ID, { appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID }, function (error) {
            expect(error).to.not.be.ok();
            expect(scope1.isDone()).to.be.ok();
            expect(scope2.isDone()).to.be.ok();

            done();
        });
    });
});
