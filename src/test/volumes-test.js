/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var async = require('async'),
    BoxError = require('../boxerror.js'),
    database = require('../database.js'),
    expect = require('expect.js'),
    volumes = require('../volumes.js');

const AUDIT_SOURCE = { ip: '1.2.3.4', userId: 'someuserid' };

function setup(done) {
    // ensure data/config/mount paths
    async.series([
        database.initialize,
        database._clear
    ], done);
}

function cleanup(done) {
    async.series([
        database._clear,
        database.uninitialize
    ], done);
}

describe('Volumes', function () {
    before(setup);
    after(cleanup);
    let volume;

    it('cannot add bad name', function (done) {
        volumes.add('music/is', '/tmp/music', AUDIT_SOURCE, function (error) {
            expect(error.reason).to.be(BoxError.BAD_FIELD);
            done();
        });
    });

    it('cannot add bad path', function (done) {
        volumes.add('music', '/tmp/music', AUDIT_SOURCE, function (error) {
            expect(error.reason).to.be(BoxError.BAD_FIELD);
            done();
        });
    });

    it('can add volume', function (done) {
        volumes.add('music', '/mnt/music', AUDIT_SOURCE, function (error, id) {
            expect(error).to.be(null);
            expect(id).to.be.a('string');
            volume = { id, name: 'music', hostPath: '/mnt/music' };
            done();
        });
    });

    it('cannot add duplicate path', function (done) {
        volumes.add('music-dup', '/mnt/music', AUDIT_SOURCE, function (error) {
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
            done();
        });
    });

    it('cannot add duplicate name', function (done) {
        volumes.add('music', '/media/music', AUDIT_SOURCE, function (error) {
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
            done();
        });
    });

    it('can get volume', function (done) {
        volumes.get(volume.id, function (error, result) {
            expect(error).to.be(null);
            expect(result.hostPath).to.be('/mnt/music');
            done();
        });
    });

    it('cannot get random volume', function (done) {
        volumes.get('randomvolume', function (error) {
            expect(error.reason).to.be(BoxError.NOT_FOUND);
            done();
        });
    });

    it('can list volumes', function (done) {
        volumes.list(function (error, result) {
            expect(error).to.be(null);
            expect(result).to.be.an(Array);
            expect(result.length).to.be(1);
            expect(result[0].id).to.be(volume.id);
            expect(result[0].hostPath).to.be('/mnt/music');
            done();
        });
    });

    it('cannot del random volume', function (done) {
        volumes.get('randomvolume', function (error) {
            expect(error.reason).to.be(BoxError.NOT_FOUND);
            done();
        });
    });

    it('can del volume', function (done) {
        volumes.del(volume, AUDIT_SOURCE, function (error) {
            expect(error).to.be(null);
            done();
        });
    });
});