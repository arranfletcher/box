'use strict';

exports = module.exports = {
    add,
    get,
    del,
    list
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    debug = require('debug')('box:volumes'),
    eventlog = require('./eventlog.js'),
    sftp = require('./sftp.js'),
    uuid = require('uuid'),
    volumedb = require('./volumedb.js');

function validateName(name) {
    assert.strictEqual(typeof name, 'string');

    if (!/^[-\w^&'@{}[\],$=!#().%+~ ]+$/.test(name)) return new BoxError(BoxError.BAD_FIELD, 'Invalid name');

    return null;
}

function validateHostPath(hostPath) {
    assert.strictEqual(typeof hostPath, 'string');

    if (!hostPath.startsWith('/mnt') && !hostPath.startsWith('/media')) return new BoxError(BoxError.BAD_FIELD, 'hostPath must be in /mnt or /media');

    return null;
}

function add(name, hostPath, auditSource, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof hostPath, 'string');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    let error = validateName(name);
    if (error) return callback(error);

    error = validateHostPath(hostPath);
    if (error) return callback(error);

    const id = uuid();

    volumedb.add(id, name, hostPath, function (error) {
        if (error) return callback(error);

        eventlog.add(eventlog.ACTION_VOLUME_ADD, auditSource, { id, name, hostPath });
        sftp.rebuild((error) => { if (error) debug('Unable to rebuild sftp:', error); });

        callback(null, id);
    });
}

function get(id, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof callback, 'function');

    volumedb.get(id, function (error, result) {
        if (error) return callback(error);

        callback(null, result);
    });
}

function list(callback) {
    assert.strictEqual(typeof callback, 'function');

    volumedb.list(function (error, result) {
        if (error) return callback(error);

        return callback(null, result);
    });
}

function del(volume, auditSource, callback) {
    assert.strictEqual(typeof volume, 'object');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    volumedb.del(volume.id, function (error) {
        if (error) return callback(error);

        eventlog.add(eventlog.ACTION_VOLUME_REMOVE, auditSource, { volume });
        sftp.rebuild((error) => { if (error) debug('Unable to rebuild sftp:', error); });

        return callback(null);
    });
}
