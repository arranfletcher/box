'use strict';

exports = module.exports = {
    getServerIp,
    testConfig
};

var assert = require('assert'),
    async = require('async'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:sysinfo/generic'),
    superagent = require('superagent');

function getServerIp(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (process.env.BOX_ENV === 'test') return callback(null, '127.0.0.1');

    async.retry({ times: 10, interval: 5000 }, function (callback) {
        superagent.get('https://api.cloudron.io/api/v1/helper/public_ip').timeout(30 * 1000).end(function (error, result) {
            if (error || result.statusCode !== 200) {
                debug('Error getting IP', error);
                return callback(new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IP. API server unreachable'));
            }
            if (!result.body && !result.body.ip) {
                debug('Unexpected answer. No "ip" found in response body.', result.body);
                return callback(new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IP. No IP found in response'));
            }

            callback(null, result.body.ip);
        });
    }, function (error, result) {
        if (error) return callback(error);

        callback(null, result);
    });
}

function testConfig(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    callback(null);
}
