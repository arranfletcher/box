'use strict';

module.exports = exports = {
    add: add,
    get: get,
    getAll: getAll,
    update: update,
    del: del,
    clear: clear,

    fqdn: fqdn,
    getName: getName,

    getDnsRecords: getDnsRecords,
    upsertDnsRecords: upsertDnsRecords,
    removeDnsRecords: removeDnsRecords,

    waitForDnsRecord: waitForDnsRecord,

    removePrivateFields: removePrivateFields,
    removeRestrictedFields: removeRestrictedFields,

    validateHostname: validateHostname,

    makeWildcard: makeWildcard,

    parentDomain: parentDomain,

    checkDnsRecords: checkDnsRecords
};

var assert = require('assert'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    debug = require('debug')('box:domains'),
    domaindb = require('./domaindb.js'),
    eventlog = require('./eventlog.js'),
    mail = require('./mail.js'),
    reverseProxy = require('./reverseproxy.js'),
    safe = require('safetydance'),
    settings = require('./settings.js'),
    sysinfo = require('./sysinfo.js'),
    tld = require('tldjs'),
    util = require('util'),
    _ = require('underscore');

const NOOP_CALLBACK = function (error) { if (error) debug(error); };

// choose which subdomain backend we use for test purpose we use route53
function api(provider) {
    assert.strictEqual(typeof provider, 'string');

    switch (provider) {
    case 'cloudflare': return require('./dns/cloudflare.js');
    case 'route53': return require('./dns/route53.js');
    case 'gcdns': return require('./dns/gcdns.js');
    case 'digitalocean': return require('./dns/digitalocean.js');
    case 'gandi': return require('./dns/gandi.js');
    case 'godaddy': return require('./dns/godaddy.js');
    case 'linode': return require('./dns/linode.js');
    case 'namecom': return require('./dns/namecom.js');
    case 'namecheap': return require('./dns/namecheap.js');
    case 'noop': return require('./dns/noop.js');
    case 'manual': return require('./dns/manual.js');
    case 'wildcard': return require('./dns/wildcard.js');
    default: return null;
    }
}

function parentDomain(domain) {
    assert.strictEqual(typeof domain, 'string');
    return domain.replace(/^\S+?\./, ''); // +? means non-greedy
}

function verifyDnsConfig(dnsConfig, domain, zoneName, provider, callback) {
    assert(dnsConfig && typeof dnsConfig === 'object'); // the dns config to test with
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof zoneName, 'string');
    assert.strictEqual(typeof provider, 'string');
    assert.strictEqual(typeof callback, 'function');

    var backend = api(provider);
    if (!backend) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid provider', { field: 'provider' }));

    const domainObject = { config: dnsConfig, domain: domain, zoneName: zoneName };
    api(provider).verifyDnsConfig(domainObject, function (error, result) {
        if (error && error.reason === BoxError.ACCESS_DENIED) return callback(new BoxError(BoxError.BAD_FIELD, `Access denied: ${error.message}`));
        if (error && error.reason === BoxError.NOT_FOUND) return callback(new BoxError(BoxError.BAD_FIELD, `Zone not found: ${error.message}`));
        if (error && error.reason === BoxError.EXTERNAL_ERROR) return callback(new BoxError(BoxError.BAD_FIELD, `Configuration error: ${error.message}`));
        if (error) return callback(error);

        callback(null, result);
    });
}

function fqdn(location, domainObject) {
    return location + (location ? '.' : '') + domainObject.domain;
}

// Hostname validation comes from RFC 1123 (section 2.1)
// Domain name validation comes from RFC 2181 (Name syntax)
// https://en.wikipedia.org/wiki/Hostname#Restrictions_on_valid_host_names
// We are validating the validity of the location-fqdn as host name (and not dns name)
function validateHostname(location, domainObject) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domainObject, 'object');

    const hostname = fqdn(location, domainObject);

    const RESERVED_LOCATIONS = [
        constants.SMTP_LOCATION,
        constants.IMAP_LOCATION
    ];
    if (RESERVED_LOCATIONS.indexOf(location) !== -1) return new BoxError(BoxError.BAD_FIELD, location + ' is reserved', { field: 'location' });

    if (hostname === settings.adminFqdn()) return new BoxError(BoxError.BAD_FIELD, location + ' is reserved', { field: 'location' });

    // workaround https://github.com/oncletom/tld.js/issues/73
    var tmp = hostname.replace('_', '-');
    if (!tld.isValid(tmp)) return new BoxError(BoxError.BAD_FIELD, 'Hostname is not a valid domain name', { field: 'location' });

    if (hostname.length > 253) return new BoxError(BoxError.BAD_FIELD, 'Hostname length exceeds 253 characters', { field: 'location' });

    if (location) {
        // label validation
        if (location.split('.').some(function (p) { return p.length > 63 || p.length < 1; })) return new BoxError(BoxError.BAD_FIELD, 'Invalid subdomain length', { field: 'location' });
        if (location.match(/^[A-Za-z0-9-.]+$/) === null) return new BoxError(BoxError.BAD_FIELD, 'Subdomain can only contain alphanumeric, hyphen and dot', { field: 'location' });
        if (/^[-.]/.test(location)) return new BoxError(BoxError.BAD_FIELD, 'Subdomain cannot start or end with hyphen or dot', { field: 'location' });
    }

    return null;
}

function validateTlsConfig(tlsConfig, dnsProvider) {
    assert.strictEqual(typeof tlsConfig, 'object');
    assert.strictEqual(typeof dnsProvider, 'string');

    switch (tlsConfig.provider) {
    case 'letsencrypt-prod':
    case 'letsencrypt-staging':
    case 'fallback':
        break;
    default:
        return new BoxError(BoxError.BAD_FIELD, 'tlsConfig.provider must be fallback, letsencrypt-prod/staging', { field: 'tlsProvider' });
    }

    if (tlsConfig.wildcard) {
        if (!tlsConfig.provider.startsWith('letsencrypt')) return new BoxError(BoxError.BAD_FIELD, 'wildcard can only be set with letsencrypt', { field: 'wildcard' });
        if (dnsProvider === 'manual' || dnsProvider === 'noop' || dnsProvider === 'wildcard') return new BoxError(BoxError.BAD_FIELD, 'wildcard cert requires a programmable DNS backend', { field: 'tlsProvider' });
    }

    return null;
}

function add(domain, data, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof data.zoneName, 'string');
    assert.strictEqual(typeof data.provider, 'string');
    assert.strictEqual(typeof data.config, 'object');
    assert.strictEqual(typeof data.fallbackCertificate, 'object');
    assert.strictEqual(typeof data.tlsConfig, 'object');
    assert.strictEqual(typeof callback, 'function');

    let { zoneName, provider, config, fallbackCertificate, tlsConfig, dkimSelector } = data;

    if (!tld.isValid(domain)) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid domain', { field: 'domain' }));
    if (domain.endsWith('.')) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid domain', { field: 'domain' }));

    if (zoneName) {
        if (!tld.isValid(zoneName)) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid zoneName', { field: 'zoneName' }));
        if (zoneName.endsWith('.')) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid zoneName', { field: 'zoneName' }));
    } else {
        zoneName = tld.getDomain(domain) || domain;
    }

    if (fallbackCertificate) {
        let error = reverseProxy.validateCertificate('test', { domain, config }, fallbackCertificate);
        if (error) return callback(error);
    } else {
        fallbackCertificate = reverseProxy.generateFallbackCertificateSync({ domain, config });
        if (fallbackCertificate.error) return callback(error);
    }

    let error = validateTlsConfig(tlsConfig, provider);
    if (error) return callback(error);

    if (!dkimSelector) dkimSelector = 'cloudron-' + settings.adminDomain().replace(/\./g, '');

    verifyDnsConfig(config, domain, zoneName, provider, function (error, sanitizedConfig) {
        if (error) return callback(error);

        domaindb.add(domain, { zoneName, provider, config: sanitizedConfig, tlsConfig, dkimSelector }, function (error) {
            if (error) return callback(error);

            reverseProxy.setFallbackCertificate(domain, fallbackCertificate, function (error) {
                if (error) return callback(error);

                eventlog.add(eventlog.ACTION_DOMAIN_ADD, auditSource, { domain, zoneName, provider });

                mail.onDomainAdded(domain, NOOP_CALLBACK);

                callback();
            });
        });
    });
}

function get(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    domaindb.get(domain, function (error, result) {
        if (error) return callback(error);

        reverseProxy.getFallbackCertificate(domain, function (_, bundle) { // never returns an error
            var cert = safe.fs.readFileSync(bundle.certFilePath, 'utf-8');
            var key = safe.fs.readFileSync(bundle.keyFilePath, 'utf-8');

            // do not error here. otherwise, there is no way to fix things up from the UI
            if (!cert || !key) debug(`Unable to read fallback certificates of ${domain} from disk`);

            result.fallbackCertificate = { cert: cert, key: key };

            return callback(null, result);
        });
    });
}

function getAll(callback) {
    assert.strictEqual(typeof callback, 'function');

    domaindb.getAll(function (error, result) {
        if (error) return callback(error);

        return callback(null, result);
    });
}

function update(domain, data, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof data.zoneName, 'string');
    assert.strictEqual(typeof data.provider, 'string');
    assert.strictEqual(typeof data.config, 'object');
    assert.strictEqual(typeof data.fallbackCertificate, 'object');
    assert.strictEqual(typeof data.tlsConfig, 'object');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    let { zoneName, provider, config, fallbackCertificate, tlsConfig } = data;

    if (settings.isDemo() && (domain === settings.adminDomain())) return callback(new BoxError(BoxError.CONFLICT, 'Not allowed in demo mode'));

    domaindb.get(domain, function (error, domainObject) {
        if (error) return callback(error);

        if (zoneName) {
            if (!tld.isValid(zoneName)) return callback(new BoxError(BoxError.BAD_FIELD, 'Invalid zoneName', { field: 'zoneName' }));
        } else {
            zoneName = domainObject.zoneName;
        }

        if (fallbackCertificate) {
            let error = reverseProxy.validateCertificate('test', domainObject, fallbackCertificate);
            if (error) return callback(error);
        }

        error = validateTlsConfig(tlsConfig, provider);
        if (error) return callback(error);

        if (provider === domainObject.provider) api(provider).injectPrivateFields(config, domainObject.config);

        verifyDnsConfig(config, domain, zoneName, provider, function (error, sanitizedConfig) {
            if (error) return callback(error);

            let newData = {
                config: sanitizedConfig,
                zoneName: zoneName,
                provider: provider,
                tlsConfig: tlsConfig
            };

            domaindb.update(domain, newData, function (error) {
                if (error) return callback(error);

                if (!fallbackCertificate) return callback();

                reverseProxy.setFallbackCertificate(domain, fallbackCertificate, function (error) {
                    if (error) return callback(error);

                    eventlog.add(eventlog.ACTION_DOMAIN_UPDATE, auditSource, { domain, zoneName, provider });

                    callback();
                });
            });
        });
    });
}

function del(domain, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (domain === settings.adminDomain()) return callback(new BoxError(BoxError.CONFLICT, 'Cannot remove admin domain'));
    if (domain === settings.mailDomain()) return callback(new BoxError(BoxError.CONFLICT, 'Cannot remove mail domain'));

    domaindb.del(domain, function (error) {
        if (error) return callback(error);

        eventlog.add(eventlog.ACTION_DOMAIN_REMOVE, auditSource, { domain });

        mail.onDomainRemoved(domain, NOOP_CALLBACK);

        return callback(null);
    });
}

function clear(callback) {
    assert.strictEqual(typeof callback, 'function');

    domaindb.clear(function (error) {
        if (error) return callback(error);

        return callback(null);
    });
}

// returns the 'name' that needs to be inserted into zone
function getName(domain, location, type) {
    const part = domain.domain.slice(0, -domain.zoneName.length - 1);

    if (location === '') return part;

    return part ? `${location}.${part}` : location;
}

function getDnsRecords(location, domain, type, callback) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof callback, 'function');

    get(domain, function (error, domainObject) {
        if (error) return callback(error);

        api(domainObject.provider).get(domainObject, location, type, function (error, values) {
            if (error) return callback(error);

            callback(null, values);
        });
    });
}

function checkDnsRecords(location, domain, callback) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    getDnsRecords(location, domain, 'A', function (error, values) {
        if (error) return callback(error);

        sysinfo.getServerIp(function (error, ip) {
            if (error) return callback(error);

            if (values.length === 0) return callback(null, { needsOverwrite: false }); // does not exist
            if (values[0] === ip) return callback(null, { needsOverwrite: false }); // exists but in sync

            callback(null, { needsOverwrite: true });
        });
    });
}

// note: for TXT records the values must be quoted
function upsertDnsRecords(location, domain, type, values, callback) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(util.isArray(values));
    assert.strictEqual(typeof callback, 'function');

    debug('upsertDNSRecord: %s on %s type %s values', location, domain, type, values);

    get(domain, function (error, domainObject) {
        if (error) return callback(error);

        api(domainObject.provider).upsert(domainObject, location, type, values, function (error) {
            if (error) return callback(error);

            callback(null);
        });
    });
}

function removeDnsRecords(location, domain, type, values, callback) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(util.isArray(values));
    assert.strictEqual(typeof callback, 'function');

    debug('removeDNSRecord: %s on %s type %s values', location, domain, type, values);

    get(domain, function (error, domainObject) {
        if (error) return callback(error);

        api(domainObject.provider).del(domainObject, location, type, values, function (error) {
            if (error && error.reason !== BoxError.NOT_FOUND) return callback(error);

            callback(null);
        });
    });
}

function waitForDnsRecord(location, domain, type, value, options, callback) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof domain, 'string');
    assert(type === 'A' || type === 'TXT');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }
    assert.strictEqual(typeof callback, 'function');

    get(domain, function (error, domainObject) {
        if (error) return callback(error);

        // linode DNS takes ~15mins
        if (!options.interval) options.interval = domainObject.provider === 'linode' ? 20000 : 5000;

        api(domainObject.provider).wait(domainObject, location, type, value, options, callback);
    });
}

// removes all fields that are strictly private and should never be returned by API calls
function removePrivateFields(domain) {
    var result = _.pick(domain, 'domain', 'zoneName', 'provider', 'config', 'tlsConfig', 'fallbackCertificate');
    return api(result.provider).removePrivateFields(result);
}

// removes all fields that are not accessible by a normal user
function removeRestrictedFields(domain) {
    var result = _.pick(domain, 'domain', 'zoneName', 'provider');

    result.config = {}; // always ensure config object

    return result;
}

function makeWildcard(hostname) {
    assert.strictEqual(typeof hostname, 'string');

    let parts = hostname.split('.');
    parts[0] = '*';
    return parts.join('.');
}
