/* jslint node:true */

'use strict';

exports = module.exports = {
    add,
    get,
    list,
    update,
    del,
    clear
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    database = require('./database.js');

const VOLUMES_FIELDS = [ 'id', 'name', 'hostPath', 'creationTime' ].join(',');

function get(id, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query(`SELECT ${VOLUMES_FIELDS} FROM volumes WHERE id=?`, [ id ], function (error, result) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Volume not found'));

        callback(null, result[0]);
    });
}

function list(callback) {
    database.query(`SELECT ${VOLUMES_FIELDS} FROM volumes ORDER BY name`, function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null, results);
    });
}

function add(id, name, hostPath, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof hostPath, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('INSERT INTO volumes (id, name, hostPath) VALUES (?, ?, ?)', [ id, name, hostPath ], function (error) {
        if (error && error.code === 'ER_DUP_ENTRY' && error.sqlMessage.indexOf('name') !== -1) return callback(new BoxError(BoxError.ALREADY_EXISTS, 'name already exists'));
        if (error && error.code === 'ER_DUP_ENTRY' && error.sqlMessage.indexOf('hostPath') !== -1) return callback(new BoxError(BoxError.ALREADY_EXISTS, 'hostPath already exists'));
        if (error && error.code === 'ER_DUP_ENTRY' && error.sqlMessage.indexOf('PRIMARY') !== -1) return callback(new BoxError(BoxError.ALREADY_EXISTS, 'id already exists'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function update(id, data, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof data, 'object');
    assert.strictEqual(typeof callback, 'function');

    var args = [ ], fields = [ ];
    for (var k in data) {
        fields.push(k + ' = ?');
        args.push(data[k]);
    }
    args.push(id);

    database.query('UPDATE volumes SET ' + fields.join(', ') + ' WHERE id=?', args, function (error) {
        if (error && error.reason === BoxError.NOT_FOUND) return callback(new BoxError(BoxError.NOT_FOUND, 'Volume not found'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function del(id, callback) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('DELETE FROM volumes WHERE id=?', [ id ], function (error, result) {
        if (error && error.code === 'ER_ROW_IS_REFERENCED_2') return callback(new BoxError(BoxError.CONFLICT, 'Volume is in use'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.affectedRows !== 1) return callback(new BoxError(BoxError.NOT_FOUND, 'Volume not found'));

        callback(null);
    });
}

function clear(callback) {
    database.query('DELETE FROM volumes', function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(error);
    });
}
