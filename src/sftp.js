'use strict';

exports = module.exports = {
    startSftp,
    rebuild
};

var apps = require('./apps.js'),
    assert = require('assert'),
    async = require('async'),
    debug = require('debug')('box:sftp'),
    hat = require('./hat.js'),
    infra = require('./infra_version.js'),
    safe = require('safetydance'),
    shell = require('./shell.js'),
    volumes = require('./volumes.js'),
    _ = require('underscore');

function startSftp(existingInfra, callback) {
    assert.strictEqual(typeof existingInfra, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (existingInfra.version === infra.version && infra.images.sftp.tag === existingInfra.images.sftp.tag) return callback();

    rebuild(callback);
}

var rebuildInProgress = false;
function rebuild(callback) {
    assert.strictEqual(typeof callback, 'function');

    if (rebuildInProgress) {
        debug('waiting for other rebuild to finish');
        return setTimeout(function () { rebuild(callback); }, 5000);
    }

    rebuildInProgress = true;

    function done(error) {
        rebuildInProgress = false;
        callback(error);
    }

    debug('rebuilding container');

    const tag = infra.images.sftp.tag;
    const memoryLimit = 256;
    const cloudronToken = hat(8 * 128);

    apps.getAll(function (error, result) {
        if (error) return done(error);

        let dataDirs = [];
        result.forEach(function (app) {
            if (!app.manifest.addons['localstorage']) return;

            const hostDir = apps.getDataDir(app, app.dataDir), mountDir = `/app/data/${app.id}`;
            if (!safe.fs.existsSync(hostDir)) {
                // do not create host path when cloudron is restoring. this will then create dir with root perms making restore logic fail
                debug(`Ignoring volume for ${app.id} since it does not exist`);
                return;
            }

            dataDirs.push({ hostDir, mountDir });
        });


        volumes.list(function (error, allVolumes) {
            if (error) return callback(error);

            allVolumes.forEach(function (volume) {
                if (!safe.fs.existsSync(volume.hostPath)) {
                    debug(`Ignoring volume host path ${volume.hostPath} since it does not exist`);
                    return;
                }

                dataDirs.push({ hostDir: volume.hostPath, mountDir: `/app/data/${volume.id}` });
            });

            shell.exec('inspectSftp', 'docker inspect --format="{{json .Mounts }}" sftp', function (error, result) {
                if (!error && result) {
                    let currentDataDirs = safe.JSON.parse(result);
                    if (currentDataDirs) {
                        currentDataDirs = currentDataDirs.filter(function (d) { return d.Destination.indexOf('/app/data/') === 0; }).map(function (d) { return { hostDir: d.Source, mountDir: d.Destination }; });

                        // sort for comparison
                        currentDataDirs.sort(function (a, b) { return a.hostDir < b.hostDir ? -1 : 1; });
                        dataDirs.sort(function (a, b) { return a.hostDir < b.hostDir ? -1 : 1; });

                        if (_.isEqual(currentDataDirs, dataDirs)) {
                            debug('Skipping rebuild, no changes');
                            return done();
                        }
                    }
                }

                const mounts = dataDirs.map(function (v) { return `-v "${v.hostDir}:${v.mountDir}"`; }).join(' ');
                const cmd = `docker run --restart=always -d --name="sftp" \
                            --hostname sftp \
                            --net cloudron \
                            --net-alias sftp \
                            --log-driver syslog \
                            --log-opt syslog-address=udp://127.0.0.1:2514 \
                            --log-opt syslog-format=rfc5424 \
                            --log-opt tag=sftp \
                            -m ${memoryLimit}m \
                            --memory-swap ${memoryLimit * 2}m \
                            --dns 172.18.0.1 \
                            --dns-search=. \
                            -p 222:22 \
                            ${mounts} \
                            -e CLOUDRON_SFTP_TOKEN="${cloudronToken}" \
                            -v "/etc/ssh:/etc/ssh:ro" \
                            --label isCloudronManaged=true \
                            --read-only -v /tmp -v /run "${tag}"`;

                // ignore error if container not found (and fail later) so that this code works across restarts
                async.series([
                    shell.exec.bind(null, 'stopSftp', 'docker stop sftp || true'),
                    shell.exec.bind(null, 'removeSftp', 'docker rm -f sftp || true'),
                    shell.exec.bind(null, 'startSftp', cmd)
                ], done);
            });
        });
    });
}
