'use strict';

let fs = require('fs'),
    path = require('path');

const CLOUDRON = process.env.BOX_ENV === 'cloudron',
    TEST = process.env.BOX_ENV === 'test';

exports = module.exports = {
    SMTP_LOCATION: 'smtp',
    IMAP_LOCATION: 'imap',

    // These are combined into one array because users and groups become mailboxes
    RESERVED_NAMES: [
        // Reserved usernames
        // https://github.com/gogits/gogs/blob/52c8f691630548fe091d30bcfe8164545a05d3d5/models/repo.go#L393
        // apps like wordpress, gogs don't like these
        // postmaster is used in dovecot and haraka
        'admin', 'no-reply', 'postmaster', 'mailer-daemon',

        // Reserved groups
        'admins', 'users'         // ldap code uses 'users' pseudo group
    ],

    ADMIN_LOCATION: 'my',

    PORT: CLOUDRON ? 3000 : 5454,
    INTERNAL_SMTP_PORT: 2525, // this value comes from the mail container
    SYSADMIN_PORT: 3001, // unused
    LDAP_PORT: 3002,
    DOCKER_PROXY_PORT: 3003,

    NGINX_DEFAULT_CONFIG_FILE_NAME: 'default.conf',

    DEFAULT_TOKEN_EXPIRATION: 365 * 24 * 60 * 60 * 1000, // 1 year

    DEFAULT_MEMORY_LIMIT: (256 * 1024 * 1024), // see also client.js

    DEMO_USERNAME: 'cloudron',
    DEMO_BLACKLISTED_APPS: [ 'com.github.cloudtorrent', 'net.alltubedownload.cloudronapp' ],

    AUTOUPDATE_PATTERN_NEVER: 'never',

    SECRET_PLACEHOLDER: String.fromCharCode(0x25CF).repeat(8), // also used in dashboard client.js

    CLOUDRON: CLOUDRON,
    TEST: TEST,

    SUPPORT_EMAIL: 'support@cloudron.io',

    FOOTER: '&copy; %YEAR%  &nbsp;  [Cloudron](https://cloudron.io) &nbsp; &nbsp; &nbsp;  [Forum <i class="fa fa-comments"></i>](https://forum.cloudron.io)',

    VERSION: process.env.BOX_ENV === 'cloudron' ? fs.readFileSync(path.join(__dirname, '../VERSION'), 'utf8').trim() : '5.1.1-test'
};

