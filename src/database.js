'use strict';

exports = module.exports = {
    initialize: initialize,
    uninitialize: uninitialize,
    query: query,
    transaction: transaction,

    importFromFile: importFromFile,
    exportToFile: exportToFile,

    _clear: clear
};

var assert = require('assert'),
    async = require('async'),
    BoxError = require('./boxerror.js'),
    child_process = require('child_process'),
    constants = require('./constants.js'),
    debug = require('debug')('box:database'),
    mysql = require('mysql'),
    once = require('once'),
    util = require('util');

var gConnectionPool = null;

const gDatabase = {
    hostname: '127.0.0.1',
    username: 'root',
    password: 'password',
    port: 3306,
    name: 'box'
};

function initialize(callback) {
    assert.strictEqual(typeof callback, 'function');

    if (gConnectionPool !== null) return callback(null);

    if (constants.TEST) {
        // see setupTest script how the mysql-server is run
        gDatabase.hostname = require('child_process').execSync('docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" mysql-server').toString().trim();
    }

    // https://github.com/mysqljs/mysql#pool-options
    gConnectionPool  = mysql.createPool({
        connectionLimit: 5,
        host: gDatabase.hostname,
        user: gDatabase.username,
        password: gDatabase.password,
        port: gDatabase.port,
        database: gDatabase.name,
        multipleStatements: false,
        waitForConnections: true, // getConnection() will wait until a connection is avaiable
        ssl: false,
        timezone: 'Z' // mysql follows the SYSTEM timezone. on Cloudron, this is UTC
    });

    gConnectionPool.on('connection', function (connection) {
        // connection objects are re-used. so we have to attach to the event here (once) to prevent crash
        // note the pool also has an 'acquire' event but that is called whenever we do a getConnection()
        connection.on('error', (error) => debug(`Connection ${connection.threadId} error: ${error.message} ${error.code}`));

        connection.query('USE ' + gDatabase.name);
        connection.query('SET SESSION sql_mode = \'strict_all_tables\'');
    });

    callback(null);
}

function uninitialize(callback) {
    if (!gConnectionPool) return callback(null);

    gConnectionPool.end(callback);
    gConnectionPool = null;
}

function clear(callback) {
    assert.strictEqual(typeof callback, 'function');

    var cmd = util.format('mysql --host="%s" --user="%s" --password="%s" -Nse "SHOW TABLES" %s | grep -v "^migrations$" | while read table; do mysql --host="%s" --user="%s" --password="%s" -e "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE $table" %s; done',
        gDatabase.hostname, gDatabase.username, gDatabase.password, gDatabase.name,
        gDatabase.hostname, gDatabase.username, gDatabase.password, gDatabase.name);

    child_process.exec(cmd, callback);
}

function query() {
    const args = Array.prototype.slice.call(arguments);
    const callback = args[args.length - 1];
    assert.strictEqual(typeof callback, 'function');

    if (constants.TEST && !gConnectionPool) return callback(new BoxError(BoxError.DATABASE_ERROR, 'database.js not initialized'));

    gConnectionPool.query.apply(gConnectionPool, args); // this is same as getConnection/query/release
}

function transaction(queries, callback) {
    assert(util.isArray(queries));
    assert.strictEqual(typeof callback, 'function');

    callback = once(callback);

    gConnectionPool.getConnection(function (error, connection) {
        if (error) return callback(error);

        const releaseConnection = (error) => { connection.release(); callback(error); };

        connection.beginTransaction(function (error) {
            if (error) return releaseConnection(error);

            async.mapSeries(queries, function iterator(query, done) {
                connection.query(query.query, query.args, done);
            }, function seriesDone(error, results) {
                if (error) return connection.rollback(() => releaseConnection(error));

                connection.commit(function (error) {
                    if (error) return connection.rollback(() => releaseConnection(error));

                    connection.release();

                    callback(null, results);
                });
            });
        });
    });
}

function importFromFile(file, callback) {
    assert.strictEqual(typeof file, 'string');
    assert.strictEqual(typeof callback, 'function');

    var cmd = `/usr/bin/mysql -h "${gDatabase.hostname}" -u ${gDatabase.username} -p${gDatabase.password} ${gDatabase.name} < ${file}`;

    async.series([
        query.bind(null, 'CREATE DATABASE IF NOT EXISTS box'),
        child_process.exec.bind(null, cmd)
    ], callback);
}

function exportToFile(file, callback) {
    assert.strictEqual(typeof file, 'string');
    assert.strictEqual(typeof callback, 'function');

    // latest mysqldump enables column stats by default which is not present in MySQL 5.7 server
    // this option must not be set in production cloudrons which still use the old mysqldump
    const disableColStats = (constants.TEST && require('fs').readFileSync('/etc/lsb-release', 'utf-8').includes('20.04')) ? '--column-statistics=0' : '';

    var cmd = `/usr/bin/mysqldump -h "${gDatabase.hostname}" -u root -p${gDatabase.password} ${disableColStats} --single-transaction --routines --triggers ${gDatabase.name} > "${file}"`;

    child_process.exec(cmd, callback);
}
