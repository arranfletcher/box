'use strict';

exports = module.exports = {
    add,
    get,
    del,
    list,
    load
};

const assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    volumes = require('../volumes.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function load(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    volumes.get(req.params.id, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        req.resource = result;

        next();
    });
}

function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));
    if (typeof req.body.hostPath !== 'string') return next(new HttpError(400, 'hostPath must be a string'));

    volumes.add(req.body.name, req.body.hostPath, auditSource.fromRequest(req), function (error, id) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, { id }));
    });

}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    next(new HttpSuccess(200, req.resource));
}

function del(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    volumes.del(req.resource, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function list(req, res, next) {
    volumes.list(function (error, result) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(200, { volumes: result }));
    });
}
