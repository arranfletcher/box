'use strict';

exports = module.exports = {
    get: get,
    list: list,
    create: create,
    update: update,
    remove: remove,
    updateMembers: updateMembers
};

var assert = require('assert'),
    BoxError = require('../boxerror.js'),
    groups = require('../groups.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function create(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be string'));

    var source = ''; // means local

    groups.create(req.body.name, source, function (error, group) {
        if (error) return next(BoxError.toHttpError(error));

        var groupInfo = {
            id: group.id,
            name: group.name
        };

        next(new HttpSuccess(201, groupInfo));
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    groups.getWithMembers(req.params.groupId, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, result));
    });
}

function update(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if ('name' in req.body && typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));

    groups.update(req.params.groupId, req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { }));
    });
}

function updateMembers(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    if (!req.body.userIds) return next(new HttpError(404, 'missing or invalid userIds fields'));
    if (!Array.isArray(req.body.userIds)) return next(new HttpError(404, 'userIds must be an array'));

    groups.setMembers(req.params.groupId, req.body.userIds, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { }));
    });
}

function list(req, res, next) {
    groups.getAllWithMembers(function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { groups: result }));
    });
}

function remove(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    groups.remove(req.params.groupId, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}
