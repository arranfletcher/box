'use strict';

exports = module.exports = {
    authorize,
    get,
    update,
    getAvatar,
    setAvatar,
    clearAvatar,
    changePassword,
    setTwoFactorAuthenticationSecret,
    enableTwoFactorAuthentication,
    disableTwoFactorAuthentication,
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    users = require('../users.js'),
    settings = require('../settings.js'),
    _ = require('underscore');

function authorize(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    settings.getDirectoryConfig(function (error, directoryConfig) {
        if (error) return next(BoxError.toHttpError(error));

        if (directoryConfig.lockUserProfiles) return next(new HttpError(403, 'admin has disallowed users from editing profiles'));

        next();
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    next(new HttpSuccess(200, {
        id: req.user.id,
        username: req.user.username,
        email: req.user.email,
        fallbackEmail: req.user.fallbackEmail,
        displayName: req.user.displayName,
        twoFactorAuthenticationEnabled: req.user.twoFactorAuthenticationEnabled,
        role: req.user.role,
        source: req.user.source,
        avatarUrl: users.getAvatarUrlSync(req.user)
    }));
}

function update(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('fallbackEmail' in req.body && typeof req.body.fallbackEmail !== 'string') return next(new HttpError(400, 'fallbackEmail must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));

    var data = _.pick(req.body, 'email', 'fallbackEmail', 'displayName');

    users.update(req.user, data, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function setAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar is missing'));

    users.setAvatar(req.user.id, req.files.avatar.path, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function clearAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.clearAvatar(req.user.id, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function getAvatar(req, res) {
    assert.strictEqual(typeof req.params.identifier, 'string');

    res.sendFile(users.getAvatarFileSync(req.params.identifier));
}

function changePassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (typeof req.body.newPassword !== 'string') return next(new HttpError(400, 'newPassword must be a string'));

    users.setPassword(req.user, req.body.newPassword, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function setTwoFactorAuthenticationSecret(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.setTwoFactorAuthenticationSecret(req.user.id, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, { secret: result.secret, qrcode: result.qrcode }));
    });
}

function enableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (!req.body.totpToken || typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be a nonempty string'));

    users.enableTwoFactorAuthentication(req.user.id, req.body.totpToken, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function disableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.disableTwoFactorAuthentication(req.user.id, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}
