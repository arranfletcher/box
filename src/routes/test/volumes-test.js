'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

var async = require('async'),
    constants = require('../../constants.js'),
    database = require('../../database.js'),
    expect = require('expect.js'),
    server = require('../../server.js'),
    superagent = require('superagent');

var SERVER_URL = 'http://localhost:' + constants.PORT;

var USERNAME = 'superadmin', PASSWORD = 'Foobar?1337', EMAIL ='silly@me.com';
var token = null;

function setup(done) {
    async.series([
        server.start.bind(null),
        database._clear.bind(null),

        function createAdmin(callback) {
            superagent.post(SERVER_URL + '/api/v1/cloudron/activate')
                .query({ setupToken: 'somesetuptoken' })
                .send({ username: USERNAME, password: PASSWORD, email: EMAIL })
                .end(function (error, result) {
                    expect(result).to.be.ok();
                    expect(result.statusCode).to.eql(201);

                    // stash token for further use
                    token = result.body.token;

                    callback();
                });
        }
    ], done);
}

function cleanup(done) {
    database._clear(function (error) {
        expect(!error).to.be.ok();

        server.stop(done);
    });
}

describe('Volumes API', function () {
    before(setup);
    after(cleanup);
    let volumeId;

    it('cannot create volume with bad name', function (done) {
        superagent.post(SERVER_URL + '/api/v1/volumes')
            .query({ access_token: token })
            .send({ name: 'music#/ ', hostPath: '/media/music' })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(400);
                done();
            });
    });

    it('cannot create volume with bad path', function (done) {
        superagent.post(SERVER_URL + '/api/v1/volumes')
            .query({ access_token: token })
            .send({ name: 'music', hostPath: '/tmp/music' })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(400);
                done();
            });
    });

    it('can create volume', function (done) {
        superagent.post(SERVER_URL + '/api/v1/volumes')
            .query({ access_token: token })
            .send({ name: 'music', hostPath: '/media/music' })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(201);
                expect(res.body.id).to.be.a('string');
                volumeId = res.body.id;
                done();
            });
    });

    it('can list volumes', function (done) {
        superagent.get(SERVER_URL + '/api/v1/volumes')
            .query({ access_token: token })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.volumes.length).to.be(1);
                expect(res.body.volumes[0].id).to.be(volumeId);
                expect(res.body.volumes[0].hostPath).to.be('/media/music');
                done();
            });
    });

    it('cannot get non-existent volume', function (done) {
        superagent.get(SERVER_URL + '/api/v1/volumes/foobar')
            .query({ access_token: token })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(404);
                done();
            });
    });

    it('can get volume', function (done) {
        superagent.get(SERVER_URL + `/api/v1/volumes/${volumeId}`)
            .query({ access_token: token })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.id).to.be(volumeId);
                expect(res.body.hostPath).to.be('/media/music');
                done();
            });
    });

    it('can delete volume', function (done) {
        superagent.del(SERVER_URL + `/api/v1/volumes/${volumeId}`)
            .query({ access_token: token })
            .end(function (err, res) {
                expect(res.statusCode).to.equal(204);
                done();
            });
    });
});
