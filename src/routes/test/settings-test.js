'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

var async = require('async'),
    constants = require('../../constants.js'),
    database = require('../../database.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    rimraf = require('rimraf'),
    server = require('../../server.js'),
    superagent = require('superagent');

var SERVER_URL = 'http://localhost:' + constants.PORT;
var USERNAME = 'superadmin', PASSWORD = 'Foobar?1337', EMAIL ='silly@me.com';
var BACKUP_FOLDER = '/tmp/backup_test';

var token = null;

function setup(done) {
    fs.mkdirSync(BACKUP_FOLDER, { recursive: true });

    async.series([
        server.start.bind(null),
        database._clear.bind(null),

        function createAdmin(callback) {
            superagent.post(SERVER_URL + '/api/v1/cloudron/activate')
                .query({ setupToken: 'somesetuptoken' })
                .send({ username: USERNAME, password: PASSWORD, email: EMAIL })
                .end(function (error, result) {
                    expect(result).to.be.ok();
                    expect(result.statusCode).to.eql(201);

                    // stash token for further use
                    token = result.body.token;

                    callback();
                });
        }
    ], done);
}

function cleanup(done) {
    rimraf.sync(BACKUP_FOLDER);

    database._clear(function (error) {
        expect(!error).to.be.ok();

        server.stop(done);
    });
}

describe('Settings API', function () {
    before(setup);
    after(cleanup);

    describe('autoupdate_pattern', function () {
        it('can get app auto update pattern (default)', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.pattern).to.be.ok();
                    done();
                });
        });

        it('cannot set autoupdate_pattern without pattern', function (done) {
            superagent.post(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('can set autoupdate_pattern', function (done) {
            superagent.post(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .send({ pattern: '00 30 11 * * 1-5' })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    done();
                });
        });

        it('can get auto update pattern', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.pattern).to.be('00 30 11 * * 1-5');
                    done();
                });
        });

        it('can set autoupdate_pattern to never', function (done) {
            superagent.post(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .send({ pattern: constants.AUTOUPDATE_PATTERN_NEVER })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    done();
                });
        });

        it('can get auto update pattern', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.pattern).to.be(constants.AUTOUPDATE_PATTERN_NEVER);
                    done();
                });
        });

        it('cannot set invalid autoupdate_pattern', function (done) {
            superagent.post(SERVER_URL + '/api/v1/settings/autoupdate_pattern')
                .query({ access_token: token })
                .send({ pattern: '1 3 x 5 6' })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });
    });

    describe('time_zone', function () {
        it('succeeds', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/time_zone')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.timeZone).to.be('America/Los_Angeles');
                    done();
                });
        });
    });

    describe('backup_config', function () {
        // keep in sync with defaults in settings.js
        let defaultConfig = {
            provider: 'filesystem',
            backupFolder: '/var/backups',
            format: 'tgz',
            encryption: null,
            retentionPolicy: { keepWithinSecs: 2 * 24 * 60 * 60 }, // 2 days
            schedulePattern: '00 00 23 * * *' // every day at 11pm
        };

        it('can get backup_config (default)', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body).to.eql(defaultConfig);
                    done();
                });
        });

        it('cannot set backup_config without provider', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.provider;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid provider', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.provider = 'invalid provider';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config without schedulePattern', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.schedulePattern;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid schedulePattern', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.schedulePattern = 'not a pattern';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config without format', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.format;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid format', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.format = 'invalid format';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config without retentionPolicy', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.retentionPolicy;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid retentionPolicy', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = 'not an object';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with empty retentionPolicy', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = {};

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with retentionPolicy missing properties', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = { foo: 'bar' };

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with retentionPolicy with invalid keepWithinSecs', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = { keepWithinSecs: 'not a number' };

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid password', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.password = 1234;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid syncConcurrency', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.syncConcurrency = 'not a number';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid syncConcurrency', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.syncConcurrency = 0;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('cannot set backup_config with invalid acceptSelfSignedCerts', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.acceptSelfSignedCerts = 'not a boolean';

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });

        it('can set backup_config', function (done) {
            var tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.format = 'rsync';
            tmp.backupFolder = BACKUP_FOLDER;

            superagent.post(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .send(tmp)
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    done();
                });
        });

        it('can get backup_config', function (done) {
            superagent.get(SERVER_URL + '/api/v1/settings/backup_config')
                .query({ access_token: token })
                .end(function (err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.format).to.equal('rsync');
                    expect(res.body.backupFolder).to.equal(BACKUP_FOLDER);
                    done();
                });
        });
    });
});
