'use strict';

exports = module.exports = {
    getDomain,

    setDnsRecords,

    getStatus,

    setMailFromValidation,
    setCatchAllAddress,
    setMailRelay,
    setMailEnabled,
    setBanner,

    sendTestMail,

    listMailboxes,
    getMailbox,
    addMailbox,
    updateMailbox,
    removeMailbox,

    getAliases,
    setAliases,

    getLists,
    getList,
    addList,
    updateList,
    removeList,

    getMailboxCount
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    mail = require('../mail.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function getDomain(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    mail.getDomain(req.params.domain, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, mail.removePrivateFields(result)));
    });
}

function setDnsRecords(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.params.domain, 'string');

    // can take a setup all the DNS entries. this is mostly because some backends try to list DNS entries (DO)
    // for upsert and this takes a lot of time
    req.clearTimeout();

    mail.setDnsRecords(req.params.domain, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201));
    });
}

function getStatus(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    // can take a while to query all the DNS entries
    req.clearTimeout();

    mail.getStatus(req.params.domain, function (error, records) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, records));
    });
}

function setMailFromValidation(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled is required'));

    mail.setMailFromValidation(req.params.domain, req.body.enabled, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function setCatchAllAddress(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.addresses) return next(new HttpError(400, 'addresses is required'));
    if (!Array.isArray(req.body.addresses)) return next(new HttpError(400, 'addresses must be an array of strings'));

    for (var i = 0; i < req.body.addresses.length; i++) {
        if (typeof req.body.addresses[i] !== 'string') return next(new HttpError(400, 'addresses must be an array of strings'));
    }

    mail.setCatchAllAddress(req.params.domain, req.body.addresses, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function setMailRelay(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if ('host' in req.body && typeof req.body.host !== 'string') return next(new HttpError(400, 'host must be a string'));
    if ('port' in req.body && typeof req.body.port !== 'number') return next(new HttpError(400, 'port must be a string'));
    if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username must be a string'));
    if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be a string'));
    if ('acceptSelfSignedCerts' in req.body && typeof req.body.acceptSelfSignedCerts !== 'boolean') return next(new HttpError(400, 'acceptSelfSignedCerts must be a boolean'));

    mail.setMailRelay(req.params.domain, req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function setMailEnabled(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled is required'));

    mail.setMailEnabled(req.params.domain, !!req.body.enabled, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function sendTestMail(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.to || typeof req.body.to !== 'string') return next(new HttpError(400, 'to must be a non-empty string'));

    mail.sendTestMail(req.params.domain, req.body.to, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function listMailboxes(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    var page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a positive number'));

    var perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a positive number'));

    if (req.query.search && typeof req.query.search !== 'string') return next(new HttpError(400, 'search must be a string'));

    mail.listMailboxes(req.params.domain, req.query.search || null, page, perPage, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { mailboxes: result }));
    });
}

function getMailboxCount(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    mail.getMailboxCount(req.params.domain, function (error, count) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { count }));
    });
}

function getMailbox(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    mail.getMailbox(req.params.name, req.params.domain, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { mailbox: result }));
    });
}

function addMailbox(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));
    if (typeof req.body.userId !== 'string') return next(new HttpError(400, 'userId must be a string'));

    mail.addMailbox(req.body.name, req.params.domain, req.body.userId, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, {}));
    });
}

function updateMailbox(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    if (typeof req.body.userId !== 'string') return next(new HttpError(400, 'userId must be a string'));

    mail.updateMailboxOwner(req.params.name, req.params.domain, req.body.userId, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function removeMailbox(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    if (typeof req.body.deleteMails !== 'boolean') return next(new HttpError(400, 'deleteMails must be a boolean'));

    mail.removeMailbox(req.params.name, req.params.domain, req.body, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, {}));
    });
}

function getAliases(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    mail.getAliases(req.params.name, req.params.domain, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { aliases: result }));
    });
}

function setAliases(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (!Array.isArray(req.body.aliases)) return next(new HttpError(400, 'aliases must be an array'));

    for (let alias of req.body.aliases) {
        if (!alias || typeof alias !== 'object') return next(new HttpError(400, 'each alias must have a name and domain'));
        if (typeof alias.name !== 'string') return next(new HttpError(400, 'name must be a string'));
        if (typeof alias.domain !== 'string') return next(new HttpError(400, 'domain must be a string'));
    }

    mail.setAliases(req.params.name, req.params.domain, req.body.aliases, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function setBanner(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.text !== 'string') return res.status(400).send({ message: 'text must be a string' });
    if ('html' in req.body && typeof req.body.html !== 'string') return res.status(400).send({ message: 'html must be a string' });

    mail.setBanner(req.params.domain, { text: req.body.text, html: req.body.html || null }, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202));
    });
}

function getLists(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    const page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a positive number'));

    const perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a positive number'));

    if (req.query.search && typeof req.query.search !== 'string') return next(new HttpError(400, 'search must be a string'));

    mail.getLists(req.params.domain, req.query.search || null, page, perPage, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { lists: result }));
    });
}

function getList(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    mail.getList(req.params.name, req.params.domain, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { list: result }));
    });
}

function addList(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));
    if (!Array.isArray(req.body.members)) return next(new HttpError(400, 'members must be a string'));
    if (req.body.members.length === 0) return next(new HttpError(400, 'list must have atleast one member'));

    for (var i = 0; i < req.body.members.length; i++) {
        if (typeof req.body.members[i] !== 'string') return next(new HttpError(400, 'member must be a string'));
    }
    if (typeof req.body.membersOnly !== 'boolean') return next(new HttpError(400, 'membersOnly must be a boolean'));

    mail.addList(req.body.name, req.params.domain, req.body.members, req.body.membersOnly, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, {}));
    });
}

function updateList(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    if (!Array.isArray(req.body.members)) return next(new HttpError(400, 'members must be a string'));
    if (req.body.members.length === 0) return next(new HttpError(400, 'list must have atleast one member'));

    for (var i = 0; i < req.body.members.length; i++) {
        if (typeof req.body.members[i] !== 'string') return next(new HttpError(400, 'member must be a string'));
    }
    if (typeof req.body.membersOnly !== 'boolean') return next(new HttpError(400, 'membersOnly must be a boolean'));

    mail.updateList(req.params.name, req.params.domain, req.body.members, req.body.membersOnly, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function removeList(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.params.name, 'string');

    mail.removeList(req.params.name, req.params.domain, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}
