'use strict';

exports = module.exports = {
    getBlocklist,
    setBlocklist
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    network = require('../network.js');

function getBlocklist(req, res, next) {
    network.getBlocklist(function (error, blocklist) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { blocklist }));
    });
}

function setBlocklist(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.blocklist !== 'string') return next(new HttpError(400, 'blocklist must be a string'));

    req.clearTimeout(); // can take a while if there is a lot of network ranges

    network.setBlocklist(req.body.blocklist, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}
