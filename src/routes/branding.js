'use strict';

exports = module.exports = {
    get,
    set,

    getCloudronAvatar
};

var assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    settings = require('../settings.js'),
    _ = require('underscore');

function getFooter(req, res, next) {
    settings.getFooter(function (error, footer) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { footer }));
    });
}

function setFooter(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.footer !== 'string') return next(new HttpError(400, 'footer is required'));

    settings.setFooter(req.body.footer, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function setCloudronName(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name is required'));

    settings.setCloudronName(req.body.name, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function getCloudronName(req, res, next) {
    settings.getCloudronName(function (error, name) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { name: name }));
    });
}

function setAppstoreListingConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    const listingConfig = _.pick(req.body, 'whitelist', 'blacklist');
    if (Object.keys(listingConfig).length === 0) return next(new HttpError(400, 'blacklist or whitelist is required'));

    if ('whitelist' in listingConfig) {
        if (listingConfig.whitelist !== null && !Array.isArray(listingConfig.whitelist)) return next(new HttpError(400, 'whitelist is null or an array of strings'));

        if (listingConfig.whitelist && !listingConfig.whitelist.every(id => typeof id === 'string')) return next(new HttpError(400, 'whitelist must be array of strings'));
    }

    if ('blacklist' in listingConfig) {
        if (!Array.isArray(listingConfig.blacklist)) return next(new HttpError(400, 'blacklist an array of strings'));

        if (!listingConfig.blacklist.every(id => typeof id === 'string')) return next(new HttpError(400, 'blacklist must be array of strings'));
    }

    settings.setAppstoreListingConfig(listingConfig, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function getAppstoreListingConfig(req, res, next) {
    settings.getAppstoreListingConfig(function (error, listingConfig) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, listingConfig));
    });
}

function setCloudronAvatar(req, res, next) {
    assert.strictEqual(typeof req.files, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar must be provided'));
    var avatar = safe.fs.readFileSync(req.files.avatar.path);

    settings.setCloudronAvatar(avatar, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function getCloudronAvatar(req, res, next) {
    settings.getCloudronAvatar(function (error, avatar) {
        if (error) return next(BoxError.toHttpError(error));

        // avoid caching the avatar on the client to see avatar changes immediately
        res.set('Cache-Control', 'no-cache');

        res.set('Content-Type', 'image/png');
        res.status(200).send(avatar);
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.setting, 'string');

    switch (req.params.setting) {
    case settings.APPSTORE_LISTING_CONFIG_KEY: return getAppstoreListingConfig(req, res, next);
    case settings.CLOUDRON_AVATAR_KEY: return getCloudronAvatar(req, res, next);
    case settings.CLOUDRON_NAME_KEY: return getCloudronName(req, res, next);
    case settings.FOOTER_KEY: return getFooter(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}

function set(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    switch (req.params.setting) {
    case settings.APPSTORE_LISTING_CONFIG_KEY: return setAppstoreListingConfig(req, res, next);
    case settings.CLOUDRON_AVATAR_KEY: return setCloudronAvatar(req, res, next);
    case settings.CLOUDRON_NAME_KEY: return setCloudronName(req, res, next);
    case settings.FOOTER_KEY: return setFooter(req, res, next);

    default: return next(new HttpError(404, 'No such branding'));
    }
}
