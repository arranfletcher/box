'use strict';

exports = module.exports = {
    proxy
};

var addons = require('../addons.js'),
    assert = require('assert'),
    BoxError = require('../boxerror.js'),
    middleware = require('../middleware/index.js'),
    HttpError = require('connect-lastmile').HttpError,
    url = require('url');

function proxy(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const id = req.params.id; // app id or volume id

    req.clearTimeout();

    addons.getContainerDetails('sftp', 'CLOUDRON_SFTP_TOKEN', function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        let parsedUrl = url.parse(req.url, true /* parseQueryString */);
        parsedUrl.query['access_token'] = result.token;

        req.url = url.format({ pathname: `/files/${id}${req.params[0]}`, query: parsedUrl.query }); // params[0] already contains leading '/'

        const proxyOptions = url.parse(`https://${result.ip}:3000`);
        proxyOptions.rejectUnauthorized = false;
        const fileManagerProxy = middleware.proxy(proxyOptions);

        fileManagerProxy(req, res, function (error) {
            if (!error) return next();

            if (error.code === 'ECONNREFUSED') return next(new HttpError(424, 'Unable to connect to filemanager server'));
            if (error.code === 'ECONNRESET') return next(new HttpError(424, 'Unable to query filemanager server'));

            next(new HttpError(500, error));
        });
    });
}
