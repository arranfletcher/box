'use strict';

exports = module.exports = {
    add: add,
    get: get,
    getAll: getAll,
    update: update,
    del: del,

    checkDnsRecords: checkDnsRecords,
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    domains = require('../domains.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.domain !== 'string') return next(new HttpError(400, 'domain must be a string'));
    if (typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider must be a string'));

    if (!req.body.config || typeof req.body.config !== 'object') return next(new HttpError(400, 'config must be an object'));
    if ('wildcard' in req.body.config && typeof req.body.config.wildcard !== 'boolean') return next(new HttpError(400, 'wildcard must be a boolean'));

    if ('zoneName' in req.body && typeof req.body.zoneName !== 'string') return next(new HttpError(400, 'zoneName must be a string'));
    if ('fallbackCertificate' in req.body && typeof req.body.fallbackCertificate !== 'object') return next(new HttpError(400, 'fallbackCertificate must be a object with cert and key strings'));
    if (req.body.fallbackCertificate) {
        let fallbackCertificate = req.body.fallbackCertificate;
        if (!fallbackCertificate.cert || typeof fallbackCertificate.cert !== 'string') return next(new HttpError(400, 'fallbackCertificate.cert must be a string'));
        if (!fallbackCertificate.key || typeof fallbackCertificate.key !== 'string') return next(new HttpError(400, 'fallbackCertificate.key must be a string'));
    }

    if ('tlsConfig' in req.body) {
        if (!req.body.tlsConfig || typeof req.body.tlsConfig !== 'object') return next(new HttpError(400, 'tlsConfig must be a object with a provider string property'));
        if (!req.body.tlsConfig.provider || typeof req.body.tlsConfig.provider !== 'string') return next(new HttpError(400, 'tlsConfig.provider must be a string'));
    }

    // some DNS providers like DigitalOcean take a really long time to verify credentials (https://github.com/expressjs/timeout/issues/26)
    req.clearTimeout();

    let data = {
        zoneName: req.body.zoneName || '',
        provider: req.body.provider,
        config: req.body.config,
        fallbackCertificate: req.body.fallbackCertificate || null,
        tlsConfig: req.body.tlsConfig || { provider: 'letsencrypt-prod' }
    };

    domains.add(req.body.domain, data, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, {}));
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    domains.get(req.params.domain, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, domains.removePrivateFields(result)));
    });
}

function getAll(req, res, next) {
    domains.getAll(function (error, result) {
        if (error) return next(new HttpError(500, error));

        result = result.map(domains.removeRestrictedFields);

        next(new HttpSuccess(200, { domains: result }));
    });
}

function update(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider must be an object'));

    if (!req.body.config || typeof req.body.config !== 'object') return next(new HttpError(400, 'config must be an object'));
    if ('wildcard' in req.body.config && typeof req.body.config.wildcard !== 'boolean') return next(new HttpError(400, 'wildcard must be a boolean'));

    if ('zoneName' in req.body && typeof req.body.zoneName !== 'string') return next(new HttpError(400, 'zoneName must be a string'));
    if ('fallbackCertificate' in req.body && typeof req.body.fallbackCertificate !== 'object') return next(new HttpError(400, 'fallbackCertificate must be a object with cert and key strings'));
    if (req.body.fallbackCertificate) {
        let fallbackCertificate = req.body.fallbackCertificate;
        if (!fallbackCertificate.cert || typeof fallbackCertificate.cert !== 'string') return next(new HttpError(400, 'fallbackCertificate.cert must be a string'));
        if (!fallbackCertificate.key || typeof fallbackCertificate.key !== 'string') return next(new HttpError(400, 'fallbackCertificate.key must be a string'));
    }

    if ('tlsConfig' in req.body) {
        if (!req.body.tlsConfig || typeof req.body.tlsConfig !== 'object') return next(new HttpError(400, 'tlsConfig must be a object with a provider string property'));
        if (!req.body.tlsConfig.provider || typeof req.body.tlsConfig.provider !== 'string') return next(new HttpError(400, 'tlsConfig.provider must be a string'));
    }

    // some DNS providers like DigitalOcean take a really long time to verify credentials (https://github.com/expressjs/timeout/issues/26)
    req.clearTimeout();

    let data = {
        zoneName: req.body.zoneName || '',
        provider: req.body.provider,
        config: req.body.config,
        fallbackCertificate: req.body.fallbackCertificate || null,
        tlsConfig: req.body.tlsConfig || { provider: 'letsencrypt-prod' }
    };

    domains.update(req.params.domain, data, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204, {}));
    });
}

function del(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    domains.del(req.params.domain, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function checkDnsRecords(req, res, next) {
    assert.strictEqual(typeof req.params.domain, 'string');

    if (!('subdomain' in req.query)) return next(new HttpError(400, 'subdomain is required'));

    // some DNS providers like DigitalOcean take a really long time to verify credentials (https://github.com/expressjs/timeout/issues/26)
    req.clearTimeout();

    domains.checkDnsRecords(req.query.subdomain, req.params.domain, function (error, result) {
        if (error && error.reason === BoxError.ACCESS_DENIED) return next(new HttpSuccess(200, { error: { reason: error.reason, message: error.message }}));
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { needsOverwrite: result.needsOverwrite }));
    });
}
