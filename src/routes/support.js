'use strict';

exports = module.exports = {
    createTicket: createTicket,

    getRemoteSupport: getRemoteSupport,
    enableRemoteSupport: enableRemoteSupport,

    canCreateTicket: canCreateTicket,
    canEnableRemoteSupport: canEnableRemoteSupport
};

var appstore = require('../appstore.js'),
    assert = require('assert'),
    auditSource = require('../auditsource.js'),
    constants = require('../constants.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    settings = require('../settings.js'),
    support = require('../support.js'),
    _ = require('underscore');

function canCreateTicket(req, res, next) {
    settings.getSupportConfig(function (error, supportConfig) {
        if (error) return next(new HttpError(503, error.message));

        if (!supportConfig.submitTickets) return next(new HttpError(405, 'feature disabled by admin'));

        next();
    });
}

function createTicket(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const VALID_TYPES = [ 'feedback', 'ticket', 'app_missing', 'app_error', 'upgrade_request', 'email_error' ];

    if (typeof req.body.type !== 'string' || !req.body.type) return next(new HttpError(400, 'type must be string'));
    if (VALID_TYPES.indexOf(req.body.type) === -1) return next(new HttpError(400, 'unknown type'));
    if (typeof req.body.subject !== 'string' || !req.body.subject) return next(new HttpError(400, 'subject must be string'));
    if (typeof req.body.description !== 'string' || !req.body.description) return next(new HttpError(400, 'description must be string'));
    if (req.body.appId && typeof req.body.appId !== 'string') return next(new HttpError(400, 'appId must be string'));
    if (req.body.altEmail && typeof req.body.altEmail !== 'string') return next(new HttpError(400, 'altEmail must be string'));
    if (req.body.enableSshSupport && typeof req.body.enableSshSupport !== 'boolean') return next(new HttpError(400, 'enableSshSupport must be a boolean'));

    settings.getSupportConfig(function (error, supportConfig) {
        if (error) return next(new HttpError(503, `Error getting support config: ${error.message}`));
        if (supportConfig.email !== constants.SUPPORT_EMAIL) return next(new HttpError(503, 'Sending to non-cloudron email not implemented yet'));

        appstore.createTicket(_.extend({ }, req.body, { email: req.user.email, displayName: req.user.displayName }), auditSource.fromRequest(req), function (error, result) {
            if (error) return next(new HttpError(503, `Error contacting cloudron.io: ${error.message}. Please email ${constants.SUPPORT_EMAIL}`));

            next(new HttpSuccess(201, result));
        });
    });
}

function canEnableRemoteSupport(req, res, next) {
    settings.getSupportConfig(function (error, supportConfig) {
        if (error) return next(new HttpError(503, error.message));

        if (!supportConfig.remoteSupport) return next(new HttpError(405, 'feature disabled by admin'));

        next();
    });
}

function enableRemoteSupport(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enable !== 'boolean') return next(new HttpError(400, 'enabled is required'));

    support.enableRemoteSupport(req.body.enable, auditSource.fromRequest(req), function (error) {
        if (error) return next(new HttpError(503, 'Error enabling remote support. Try running "cloudron-support --enable-ssh" on the server'));

        next(new HttpSuccess(202, {}));
    });
}

function getRemoteSupport(req, res, next) {
    support.getRemoteSupport(function (error, status) {
        if (error) return next(new HttpError(500, error));

        next(new HttpSuccess(200, status));
    });
}
