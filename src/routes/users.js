'use strict';

exports = module.exports = {
    get,
    update,
    list,
    create,
    remove,
    changePassword,
    verifyPassword,
    createInvite,
    sendInvite,
    setGroups,
    setAvatar,
    clearAvatar,

    load
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    users = require('../users.js');

function load(req, res, next) {
    assert.strictEqual(typeof req.params.userId, 'string');

    users.get(req.params.userId, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        req.resource = result;

        next();
    });
}

function create(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));
    if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be string'));
    if ('role' in req.body) {
        if (typeof req.body.role !== 'string') return next(new HttpError(400, 'role must be string'));
        if (users.compareRoles(req.user.role, req.body.role) < 0) return next(new HttpError(403, `role '${req.body.role}' is required but you are only '${req.user.role}'`));
    }

    var password = req.body.password || null;
    var email = req.body.email;
    var username = 'username' in req.body ? req.body.username : null;
    var displayName = req.body.displayName || '';

    users.create(username, password, email, displayName, { invitor: req.user, role: req.body.role || users.ROLE_USER }, auditSource.fromRequest(req), function (error, user) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, users.removePrivateFields(user)));
    });
}

function update(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('fallbackEmail' in req.body && typeof req.body.fallbackEmail !== 'string') return next(new HttpError(400, 'fallbackEmail must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));
    if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username must be a string'));

    if ('role' in req.body) {
        if (typeof req.body.role !== 'string') return next(new HttpError(400, 'role must be a string'));
        if (req.user.id === req.resource.id) return next(new HttpError(409, 'Cannot set role flag on self'));

        if (users.compareRoles(req.user.role, req.body.role) < 0) return next(new HttpError(403, `role '${req.body.role}' is required but you are only '${req.user.role}'`));
    }

    if ('active' in req.body) {
        if (typeof req.body.active !== 'boolean') return next(new HttpError(400, 'active must be a boolean'));
        if (req.user.id === req.resource.id) return next(new HttpError(409, 'Cannot set active flag on self'));
    }

    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but you are only '${req.user.role}'`));

    users.update(req.resource, req.body, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function list(req, res, next) {
    var page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    var perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    if (req.query.search && typeof req.query.search !== 'string') return next(new HttpError(400, 'search must be a string'));

    users.getAllPaged(req.query.search || null, page, perPage, function (error, results) {
        if (error) return next(BoxError.toHttpError(error));

        results = results.map(users.removeRestrictedFields);

        next(new HttpSuccess(200, { users: results }));
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');
    assert.strictEqual(typeof req.user, 'object');

    next(new HttpSuccess(200, users.removePrivateFields(req.resource)));
}

function remove(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (req.user.id === req.resource.id) return next(new HttpError(409, 'Not allowed to remove yourself.'));
    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but user has only '${req.user.role}'`));

    users.remove(req.resource, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function verifyPassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.password !== 'string') return next(new HttpError(400, 'API call requires user password'));

    users.verifyWithUsername(req.user.username, req.body.password, users.AP_WEBADMIN, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        req.body.password = '<redacted>'; // this will prevent logs from displaying plain text password

        next();
    });
}

function createInvite(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but user has only '${req.user.role}'`));

    users.createInvite(req.resource, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, result));
    });
}

function sendInvite(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but user has only '${req.user.role}'`));

    users.sendInvite(req.resource, { invitor: req.user }, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { }));
    });
}

function setGroups(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.resource, 'object');

    if (!Array.isArray(req.body.groupIds)) return next(new HttpError(400, 'API call requires a groups array.'));
    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but user has only '${req.user.role}'`));

    users.setMembership(req.resource, req.body.groupIds, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function changePassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.resource, 'object');

    if (typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be a string'));
    if (users.compareRoles(req.user.role, req.resource.role) < 0) return next(new HttpError(403, `role '${req.resource.role}' is required but user has only '${req.user.role}'`));

    users.setPassword(req.resource, req.body.password, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function setAvatar(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar is missing'));

    users.setAvatar(req.resource.id, req.files.avatar.path, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function clearAvatar(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    users.clearAvatar(req.resource.id, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}
