'use strict';

exports = module.exports = {
    accesscontrol: require('./accesscontrol.js'),
    appPasswords: require('./apppasswords.js'),
    apps: require('./apps.js'),
    appstore: require('./appstore.js'),
    backups: require('./backups.js'),
    branding: require('./branding.js'),
    cloudron: require('./cloudron.js'),
    domains: require('./domains.js'),
    eventlog: require('./eventlog.js'),
    filemanager: require('./filemanager.js'),
    graphs: require('./graphs.js'),
    groups: require('./groups.js'),
    mail: require('./mail.js'),
    mailserver: require('./mailserver.js'),
    network: require('./network.js'),
    notifications: require('./notifications.js'),
    profile: require('./profile.js'),
    provision: require('./provision.js'),
    services: require('./services.js'),
    settings: require('./settings.js'),
    support: require('./support.js'),
    tasks: require('./tasks.js'),
    tokens: require('./tokens.js'),
    users: require('./users.js'),
    volumes: require('./volumes.js')
};
