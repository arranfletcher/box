'use strict';

exports = module.exports = {
    list: list,
    startBackup: startBackup,
    cleanup: cleanup,
    check: check
};

let auditSource = require('../auditsource.js'),
    backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

function list(req, res, next) {
    var page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    var perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    backups.getByIdentifierAndStatePaged(backups.BACKUP_IDENTIFIER_BOX, backups.BACKUP_STATE_NORMAL, page, perPage, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { backups: result }));
    });
}

function startBackup(req, res, next) {
    backups.startBackupTask(auditSource.fromRequest(req), function (error, taskId) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, { taskId }));
    });
}

function cleanup(req, res, next) {
    backups.startCleanupTask(auditSource.fromRequest(req), function (error, taskId) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, { taskId }));
    });
}

function check(req, res, next) {
    backups.checkConfiguration(function (error, message) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { ok: !message, message: message }));
    });
}
