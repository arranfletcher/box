'use strict';

exports = module.exports = {
    get: get,
    list: list,
    update: update,

    clear: clear,

    TYPE_USER: 'user',
    TYPE_APP: 'app',
    TYPE_GROUP: 'group'
};

var assert = require('assert'),
    BoxError = require('./boxerror.js'),
    database = require('./database.js'),
    safe = require('safetydance');

var MAILDB_FIELDS = [ 'domain', 'enabled', 'mailFromValidation', 'catchAllJson', 'relayJson', 'dkimSelector', 'bannerJson' ].join(',');

function postProcess(data) {
    data.enabled = !!data.enabled; // int to boolean
    data.mailFromValidation = !!data.mailFromValidation; // int to boolean

    data.catchAll = safe.JSON.parse(data.catchAllJson) || [ ];
    delete data.catchAllJson;

    data.relay = safe.JSON.parse(data.relayJson) || { provider: 'cloudron-smtp' };
    delete data.relayJson;

    data.banner = safe.JSON.parse(data.bannerJson) || { text: null, html: null };
    delete data.bannerJson;

    return data;
}

function clear(callback) {
    assert.strictEqual(typeof callback, 'function');

    // using TRUNCATE makes it fail foreign key check
    database.query('DELETE FROM mail', [], function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        callback(null);
    });
}

function get(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILDB_FIELDS + ' FROM mail WHERE domain = ?', [ domain ], function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (results.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Mail domain not found'));

        callback(null, postProcess(results[0]));
    });
}

function list(callback) {
    assert.strictEqual(typeof callback, 'function');

    database.query('SELECT ' + MAILDB_FIELDS + ' FROM mail ORDER BY domain', function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        results.forEach(function (result) { postProcess(result); });

        callback(null, results);
    });
}

function update(domain, data, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof data, 'object');
    assert.strictEqual(typeof callback, 'function');

    var args = [ ];
    var fields = [ ];
    for (var k in data) {
        if (k === 'catchAll' || k === 'banner') {
            fields.push(`${k}Json = ?`);
            args.push(JSON.stringify(data[k]));
        } else if (k === 'relay') {
            fields.push('relayJson = ?');
            args.push(JSON.stringify(data[k]));
        } else {
            fields.push(k + ' = ?');
            args.push(data[k]);
        }
    }
    args.push(domain);

    database.query('UPDATE mail SET ' + fields.join(', ') + ' WHERE domain=?', args, function (error, result) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.affectedRows !== 1) return callback(new BoxError(BoxError.NOT_FOUND, 'Mail domain not found'));

        callback(null);
    });
}
