'use strict';

exports = module.exports = {
    verifyToken: verifyToken
};

var assert = require('assert'),
    BoxError = require('./boxerror.js'),
    tokendb = require('./tokendb.js'),
    users = require('./users.js');

function verifyToken(accessToken, callback) {
    assert.strictEqual(typeof accessToken, 'string');
    assert.strictEqual(typeof callback, 'function');

    tokendb.getByAccessToken(accessToken, function (error, token) {
        if (error && error.reason === BoxError.NOT_FOUND) return callback(new BoxError(BoxError.INVALID_CREDENTIALS));
        if (error) return callback(error);

        users.get(token.identifier, function (error, user) {
            if (error && error.reason === BoxError.NOT_FOUND) return callback(new BoxError(BoxError.INVALID_CREDENTIALS));
            if (error) return callback(error);

            if (!user.active) return callback(new BoxError(BoxError.INVALID_CREDENTIALS));

            callback(null, user);
        });
    });
}
