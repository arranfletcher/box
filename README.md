# Cloudron

[Cloudron](https://cloudron.io) is the best way to run apps on your server.

Web applications like email, contacts, blog, chat are the backbone of the modern
internet. Yet, we live in a world where hosting these essential applications is
a complex task.

We are building the ultimate platform for self-hosting web apps. The Cloudron allows
anyone to effortlessly host web applications on their server on their own terms.

## Features

* Single click install for apps. Check out the [App Store](https://cloudron.io/appstore.html).

* Per-app encrypted backups and restores.

* App updates delivered via the App Store.

* Secure - Cloudron manages the firewall. All apps are secured with HTTPS. Certificates are
  installed and renewed automatically.

* Centralized User & Group management. Control who can access which app.

* Single Sign On. Use same credentials across all apps.

* Automatic updates for the Cloudron platform.

* Trivially migrate to another server keeping your apps and data (for example, switch your
  infrastructure provider or move to a bigger server).

* Comprehensive [REST API](https://docs.cloudron.io/api/).

* [CLI](https://docs.cloudron.io/custom-apps/cli/) to configure apps.

* Alerts, audit logs, graphs, dns management ... and much more

## Demo

Try our demo at https://my.demo.cloudron.io (username: cloudron password: cloudron).

## Installing

[Install script](https://docs.cloudron.io/installation/) - [Pricing](https://cloudron.io/pricing.html)

**Note:** This repo is a small part of what gets installed on your server - there is
the dashboard, database addons, graph container, base image etc. Cloudron also relies
on external services such as the App Store for apps to be installed. As such, don't
clone this repo and npm install and expect something to work.

## Development

This is the backend code of Cloudron. The frontend code is [here](https://git.cloudron.io/cloudron/dashboard).

The way to develop is to first install a full instance of Cloudron in a VM. Then you can use the [hotfix](https://git.cloudron.io/cloudron/cloudron-machine)
tool to patch the VM with the latest code.

```
SSH_PASSPHRASE=sshkeypassword cloudron-machine hotfix --cloudron my.example.com --release 6.0.0 --ssh-key keyname
```

## License

Please note that the Cloudron code is under a source-available license. This is not the same as an
open source license but ensures the code is available for introspection (and hacking!).

## Contributions

Just to give some heads up, we are a bit restrictive in merging changes. We are a small team and
would like to keep our maintenance burden low. It might be best to discuss features first in the [forum](https://forum.cloudron.io),
to also figure out how many other people will use it to justify maintenance for a feature.

## Support

* [Documentation](https://docs.cloudron.io/)
* [Forum](https://forum.cloudron.io/)

