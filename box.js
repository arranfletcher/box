#!/usr/bin/env node

'use strict';

let async = require('async'),
    dockerProxy = require('./src/dockerproxy.js'),
    fs = require('fs'),
    ldap = require('./src/ldap.js'),
    paths = require('./src/paths.js'),
    server = require('./src/server.js');

const NOOP_CALLBACK = function () { };

function setupLogging(callback) {
    if (process.env.BOX_ENV === 'test') return callback();

    var logfileStream = fs.createWriteStream(paths.BOX_LOG_FILE, { flags:'a' });
    process.stdout.write = process.stderr.write = logfileStream.write.bind(logfileStream);

    callback();
}

async.series([
    setupLogging,
    server.start,
    ldap.start,
    dockerProxy.start
], function (error) {
    if (error) {
        console.log('Error starting server', error);
        process.exit(1);
    }

    // require those here so that logging handler is already setup
    require('supererror');
    const debug = require('debug')('box:box');

    process.on('SIGINT', function () {
        debug('Received SIGINT. Shutting down.');

        server.stop(NOOP_CALLBACK);
        ldap.stop(NOOP_CALLBACK);
        dockerProxy.stop(NOOP_CALLBACK);
        setTimeout(process.exit.bind(process), 3000);
    });

    process.on('SIGTERM', function () {
        debug('Received SIGTERM. Shutting down.');

        server.stop(NOOP_CALLBACK);
        ldap.stop(NOOP_CALLBACK);
        dockerProxy.stop(NOOP_CALLBACK);
        setTimeout(process.exit.bind(process), 3000);
    });

    process.on('uncaughtException', function (error) {
        console.error((error && error.stack) ? error.stack : error);
        setTimeout(process.exit.bind(process, 1), 3000);
    });

    console.log(`Cloudron is up and running. Logs are at ${paths.BOX_LOG_FILE}`); // this goes to journalctl
});
